<?php
namespace Options\Responder;

use App\Responder\HtmlResponder;

class Index extends HtmlResponder
{
    public function __invoke()
    {
        $data = [
            'layout'   => 'layout::admin',
            'template' => 'options::index'
        ];

        return $this->getResponse($data);
    } // __invoke()
}