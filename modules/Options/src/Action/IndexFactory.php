<?php
namespace Options\Action;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Options\Responder\Index as IndexResponder;

class IndexFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $template = $container->get(TemplateRendererInterface::class);
        $responder = new IndexResponder($template);

        return new Index($responder);
    } // __invoke()
}