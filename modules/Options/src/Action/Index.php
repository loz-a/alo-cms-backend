<?php
namespace Options\Action;

use Options\Responder\Index as IndexResponder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @Api.TokenVerify
 */
class Index
{
    /**
     * @var IndexResponder
     */
    private $responder;

    public function __construct(IndexResponder $responder)
    {
        $this->responder = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        return $this->responder->__invoke();
    } // __invoke()
}