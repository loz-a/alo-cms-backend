<?php
namespace Options;


class ConfigProvider
{
    const MODULE_NAME = __NAMESPACE__;

    public function __invoke()
    {
        return require __DIR__ . '/../config/module.config.php';
    } // __invoke()
}