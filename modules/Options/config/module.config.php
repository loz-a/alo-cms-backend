<?php

return [
    'dependencies' => [
        'factories' => [
            Options\Action\Index::class => Options\Action\IndexFactory::class,
        ],
    ],

    'routes' => [
        [   'name' => 'options',
            'path' => '/options',
            'middleware' => [
                Options\Action\Index::class
            ],
            'allowed_methods' => ['GET']
        ]
    ],

    'view_helpers' => [
        'factories' => [
        ]
    ],

    'templates' => [
        'map' => [
            'options::index' => __DIR__ . '/../templates/options/index.phtml'
        ],
        'paths' => [
            'options' => [ __DIR__ . '/../templates/options']
        ]
    ],

    'admin_navigation' => [
        'options' => [
            'title' => 'Site options',
            'icon'  => 'options',
            'index' => 2
        ]
    ]

];
