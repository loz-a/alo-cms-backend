<?php
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'dependencies' => [
        'invokables' => [
        ],
        'factories' => [
        ],
    ],

    'view_helpers' => [
        'aliases' => [
            'flashMessages' => App\View\Helper\FlashMessenger\Messages::class,
            'appnedModuleResources' => App\View\Helper\AppendModuleResources::class,
        ],
        'factories' => [
            App\View\Helper\FlashMessenger\Messages::class => App\View\Helper\FlashMessenger\MessagesFactory::class,
            App\View\Helper\AppendModuleResources::class   => InvokableFactory::class,
        ]
    ],

    'templates' => [
        'map' => [
            'app::messages' => __DIR__ . '/../templates/app/messages.phtml',
            'app::message'  => __DIR__ . '/../templates/app/partial/message.phtml'
        ],
        'path' => [
            'app' => [__DIR__ . '/../templates/app']
        ]
    ]

];
