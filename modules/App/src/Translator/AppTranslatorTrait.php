<?php
namespace App\Translator;


trait AppTranslatorTrait
{
    /**
     * @var \Zend\I18n\Translator\Translator
     */
    protected $translator;


    abstract protected function getTranslatorTextDomain();


    protected function translate($message)
    {
        $translatorTextDomain = $this->getTranslatorTextDomain();
        return $this->translator->translate($message, $translatorTextDomain);
    }


    protected function getTranslateMessages()
    {
        $translatorTextDomain = $this->getTranslatorTextDomain();
        $messages = $this->translator->getAllMessages($translatorTextDomain);

        if ($messages) {
            return $messages->getArrayCopy();
        }
        return [];
    }
}