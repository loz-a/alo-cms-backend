<?php
namespace App\Domain\Storage\Db;

use Zend\Db\Adapter\AdapterInterface as DbAdapterInterface;
use Zend\Db\Sql\Sql;

abstract class AbstractDbStorage
{
    /**
     * @var DbAdapterInterface
     */
    protected $dbAdapter;

    /**
     * @var string
     */
    protected $table;

    public function __construct(string $table, DbAdapterInterface $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
        $this->table = $table;
    }


    public function getTableName()
    {
        return $this->table;
    }


    protected function sql()
    {
        return new Sql($this->dbAdapter);
    }
}