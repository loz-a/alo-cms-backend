<?php
namespace App\Responder;


interface ResponderInterface
{
    public function getResponse(array $data = []);
}