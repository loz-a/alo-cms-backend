<?php
namespace App\Responder;

use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class HtmlResponder implements ResponderInterface
{
    /**
     * @var TemplateRendererInterface
     */
    private $template;

    public function __construct(TemplateRendererInterface $template)
    {
        $this->template = $template;
    } // __construct()


    public function getResponse(array $data = [])
    {
        $template = $data['template'];
        $params   = $data['template_params'] ?? [];
        $status   = $data['response_status'] ?? 200;
        $headers  = $data['response_headers'] ?? [];

        if (isset($data['layout'])) {
            $params['layout'] = $data['layout'];
        }

        return new HtmlResponse(
            $this->template->render($template, $params),
            $status,
            $headers
        );
    }

    public function pageNotFoundResponse(array $data)
    {
        $data['response_status'] = 404;
        return $this->getResponse($data);
    } // pageNotFoundResponse()

    public function errorResponse(array $data)
    {
        $data['response_status'] = 500;
        return $this->getResponse($data);
    } // errorResponse()
}