<?php

namespace App\Responder;

use Zend\Diactoros\Response\JsonResponse;

class JsonResponder implements ResponderInterface
{
    public function getResponse(array $data = [])
    {
        $status   = $data['response_status'] ?? 200;
        $headers  = $data['response_headers'] ?? [];
        $encodingOptions = $data['response_encoding_options'] ?? JsonResponse::DEFAULT_JSON_FLAGS;
        $dataData = $data['data'] ?? [];

        return new JsonResponse($dataData, $status, $headers, $encodingOptions);
    }

}