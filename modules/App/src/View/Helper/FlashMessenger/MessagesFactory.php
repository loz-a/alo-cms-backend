<?php
namespace App\View\Helper\FlashMessenger;

use Interop\Container\ContainerInterface;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class MessagesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $fm = $container->get(FlashMessenger::class);
        return new Messages($fm);
    } // __invoke()
}