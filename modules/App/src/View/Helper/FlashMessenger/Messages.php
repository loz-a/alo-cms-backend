<?php
namespace App\View\Helper\FlashMessenger;

use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Zend\View\Helper\AbstractHelper;

class Messages extends AbstractHelper
{
    /**
     * @var FlashMessenger
     */
    private $flashMessenger;

    public function __construct(FlashMessenger $flashMessenger)
    {
        $this->flashMessenger = $flashMessenger;
    } // __construct()

    public function __invoke()
    {
        return $this->getView()->render('app::messages', $this->getRenderValues());
    } // __invoke()


    protected function getRenderValues()
    {
        return [
            FlashMessenger::NAMESPACE_DEFAULT => $this->getMessagesByNamespace(),
            FlashMessenger::NAMESPACE_SUCCESS => $this->getMessagesByNamespace(FlashMessenger::NAMESPACE_SUCCESS),
            FlashMessenger::NAMESPACE_WARNING => $this->getMessagesByNamespace(FlashMessenger::NAMESPACE_WARNING),
            FlashMessenger::NAMESPACE_ERROR   => $this->getMessagesByNamespace(FlashMessenger::NAMESPACE_ERROR),
            FlashMessenger::NAMESPACE_INFO    => $this->getMessagesByNamespace(FlashMessenger::NAMESPACE_INFO)
        ];
    } // getRenderValues()

    protected function getMessagesByNamespace($namespace = null)
    {
        $messages = [];
        $fm = $this->flashMessenger;

        if ($fm->hasCurrentMessages($namespace)) {
            $messages = $fm->getCurrentMessages($namespace);
        }

        if ($fm->hasMessages($namespace)) {
            $messages = count($messages) ? array_merge($fm->getMessages($namespace), $messages) : $fm->getMessages($namespace);
        }

        return $messages;
    } // getMessagesByNamespace()
}