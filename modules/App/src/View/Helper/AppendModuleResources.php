<?php
namespace App\View\Helper;

use Zend\View\Helper\AbstractHelper;

class AppendModuleResources extends AbstractHelper
{
    protected $scriptsPath = 'scripts';

    protected $stylesPath  = 'css';

//    protected $imagesPath  = 'img';

    protected $publicPath  = 'public';

    public function __invoke(string $moduleName)
    {
        $ds       = DIRECTORY_SEPARATOR;
        $public   = $this->publicPath;
        $styles   = $this->stylesPath;
        $scripts  = $this->scriptsPath;
        $resource = strtolower($moduleName);

        $filename = getcwd() . $ds.$public.$ds.$styles.$ds.$resource.'.css';
        if (file_exists($filename)) {
            $this
                ->getView()
                ->headLink()
                ->appendStylesheet(sprintf('/%s/%s.css', $styles, $resource));
        }


        $filename = getcwd() . $ds.$public.$ds.$scripts.$ds.$resource.'.bundle.js';
        if (file_exists($filename)) {
            $this
                ->getView()
                ->inlineScript()
                ->appendFile(sprintf('/%s/%s.bundle.js', $scripts, $resource));
        }
    } // __invoke()
}