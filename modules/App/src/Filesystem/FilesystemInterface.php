<?php
namespace App\Filesystem;

interface FilesystemInterface
{
    public function read($path);

    public function has($path);

    public function put($path, $contents);

    public function delete($path);

    public function rename($old, $new);

}