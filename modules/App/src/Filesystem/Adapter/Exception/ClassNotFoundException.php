<?php
namespace App\Filesystem\Adapter\Exception;

class ClassNotFoundException extends \Exception
    implements ExceptionInterface
{

}