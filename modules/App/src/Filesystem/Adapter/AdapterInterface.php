<?php
namespace App\Filesystem\Adapter;

interface AdapterInterface
{
    public function read($path);

    public function has($path);

    public function put($path, $contents);

    public function delete($path);

    public function rename($old, $new);

    public function getFilesystem();
}