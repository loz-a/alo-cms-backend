<?php
namespace App\Filesystem;

interface AdapterAwareInterface
{
    /**
     * @return Adapter\AdapterInterface
     */
    public function getAdapter();

    /**
     * @param Adapter\AdapterInterface $adapter
     * @return mixed
     */
    public function setAdapter(Adapter\AdapterInterface $adapter);
}