<?php
namespace App;

class ConfigProvider
{
    public function __invoke()
    {
        return require __DIR__ . '/../config/module.config.php';
    } // __invoke()
}