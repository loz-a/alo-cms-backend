<?php
namespace App;


interface InvokableInterface
{
    public function __invoke(...$args);
}