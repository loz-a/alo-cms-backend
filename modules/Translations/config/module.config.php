<?php

use Translations\ConfigProvider;
//use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'dependencies' => [
        'factories' => [
            Translations\Action\Config::class => Translations\Action\ConfigFactory::class,
            Translations\Action\Index::class => Translations\Action\IndexFactory::class,
            Translations\Service\Translator\TranslatorInterface::class => Translations\Service\Translator\Factory::class,
        ]

    ],

    'routes' => [
        [   'name' => 'translations',
            'path' => '/translations',
            'middleware' => [
                Translations\Action\Index::class
            ],
            'allowed_methods' => ['GET']
        ],
        [   'name' => 'translations_config',
            'path' => '/translations-config.json',
            'middleware' => [
                Translations\Action\Config::class
            ],
            'allowed_methods' => ['GET']
        ]
    ],

    'templates' => [
        'map' => [
            'translations::index' => __DIR__ . '/../templates/translations/index.phtml'
        ],
        'paths' => [
            'translations' => [__DIR__ . '/../templates/translations']
        ]
    ],

    'translator' => [
        'scan_files' => [
            ConfigProvider::MODULE_NAME => [
                'title' => __('Translations'),
                'import' => [
                    'paths' => [ // Import translations from various sources (po, mo, php, js, etc)
                        __DIR__,
                        __DIR__ . '/../src',
                        __DIR__ . '/../templates'
                    ],
                    'extensions' => ['php', 'phtml'],
                ],
                'export' => [ // Export translations to various formats (po, mo, php, json, etc)
                    'path' => __DIR__ . '/../data/locale'
                ]
            ]
        ],
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../data/locale',
                'pattern' => '%s/LC_MESSAGES/messages.mo',
                'text_domain' => ConfigProvider::MODULE_NAME
            ]
        ]
    ],

    'admin_navigation' => [
        'translations' => [
            'title' => __('Translations'),
            'icon'  => 'translate',
            'index' => 4
        ]
    ]

];
