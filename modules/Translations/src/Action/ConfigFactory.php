<?php
namespace Translations\Action;

use Admin\AdminNavigationTrait;
use App\Responder\JsonResponder;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;

class ConfigFactory
{
    use AdminNavigationTrait;

    public function __invoke(ContainerInterface $container)
    {
        $translator = $container->get(TranslatorInterface::class);
        $navigation = $this->getAdminNavigation($container);
        $responder = new JsonResponder();

        return new Config($translator, $navigation, $responder);
    }
}