<?php
namespace Translations\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Translations\Responder\Index as IndexResponder;
use Translations\ConfigProvider;

/**
 * @Api.TokenVerify
 */
class Index
{
    /**
     * @var array
     */
    private $config;
    /**
     * @var IndexResponder
     */
    private $responder;

    public function __construct(
        \ArrayObject $config,
        IndexResponder $responder
    ){
        $this->responder = $responder;
        $this->config = $config;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){

//        $config = $this->config['translator']['scan_files'][ConfigProvider::MODULE_NAME] ?? [];
//
//        $importPaths = $config['import']['paths'] ?? [];
//        $importExtensions = $config['import']['extensions'] ?? [];
//
//        $translator = $container->get(Translations\Service\Translator\TranslatorInterface::class);
//
//        $translator
//            ->getTranslationsAdapter()
//            ->setLanguage('en_GB')
//            ->setDomain('Domain');
//
//        $translator->scanFiles($importPaths, $importExtensions);
//        $translator->exportToPoFile($config['export']['path']);
//        $translator->exportToMoFile($config['export']['path']);

//        $result = $this->scan();
        translate('Hello world');

        return $this->responder->__invoke();
    }


    public function scan()
    {

//        $config = $this->config['translator']['scan_files'][ConfigProvider::MODULE_NAME] ?? [];
//
//        // get php and phtml path files
//        $dir = new \RecursiveDirectoryIterator(getcwd().'/modules/Translations', \RecursiveDirectoryIterator::SKIP_DOTS);
//        $iterator = new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::SELF_FIRST);
//        $filter = new \RegexIterator($iterator, '/\.(?:php|phtml)$/');
//
//        $files = [];
//        foreach($filter as $entry) {
//            $files[] = $entry->getPathname();
//        }
//
//        // get config paths for translate
//        $dir = new \RecursiveDirectoryIterator(getcwd().'/modules', \RecursiveDirectoryIterator::SKIP_DOTS);
//        $iterator = new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::SELF_FIRST);
//        $filter = new \RegexIterator($iterator, '/module\.config\.php$/');
//
//        foreach($filter as $entry) {
//            $files[] = $entry->getPathname();
//        }
//
//        $files = array_map(function($path) { return str_replace(['\\', '/'], DIRECTORY_SEPARATOR, trim($path)); }, $files);
//        $files = array_keys(array_flip($files));

        // scan files
        $translations = new \Gettext\Translations();
        $translations->setDomain(ConfigProvider::MODULE_NAME);
        $translations->setLanguage('uk_UA');

        $options = [
            'extractComments' => false,
            'constants' => [],
            'functions' => array_merge(\Gettext\Extractors\PhpCode::$options['functions'], ['translate' => 'gettext'])
        ];

        foreach ($files as $file) {
            $translations->addFromPhpCodeFile($file, $options);
        }

        $translations->toPoFile(__DIR__ . '/../../data/locale/uk_UA/LC_MESSAGES/messages.po');
        $translations->toMoFile(__DIR__ . '/../../data/locale/uk_UA/LC_MESSAGES/messages.mo');

        return $translations;
    }

}