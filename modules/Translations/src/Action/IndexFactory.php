<?php
namespace Translations\Action;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Translations\Responder\Index as IndexResponder;

class IndexFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $template = $container->get(TemplateRendererInterface::class);
        $responder = new IndexResponder($template);

        return new Index($config, $responder);
    }
}