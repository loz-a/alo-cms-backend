<?php
namespace Translations\Translator;

use App\Translator\AppTranslatorTrait;
use Translations\ConfigProvider;

trait TranslatorTrait
{
    use AppTranslatorTrait;

    protected function getTranslatorTextDomain()
    {
        return  ConfigProvider::MODULE_NAME;
    }
}