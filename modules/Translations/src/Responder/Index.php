<?php
namespace Translations\Responder;

use App\Responder\HtmlResponder;

class Index extends HtmlResponder
{
    public function __invoke()
    {
        $data = [
            'template' => 'translations::index',
            'layout' => 'layout::admin'
        ];

        return $this->getResponse($data);
    }
}