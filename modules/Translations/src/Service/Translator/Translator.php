<?php
namespace Translations\Service\Translator;

use Translations\Service\Translator\TranslationsAdapter\AdapterInterface as TranslationsAdapterInterface;
use Translations\Service\PathnameResolver\ResolverInterface;
use App\Filesystem\Filesystem;
use App\Filesystem\Adapter\FlySystemAdapter;
use League\Flysystem\Adapter\Local;

class Translator
{
    /**
     * @var TranslationsAdapterInterface
     */
    protected $translationsAdapter;

    /**
     * @var ResolverInterface
     */
    protected $pathnameResolver;

    /**
     * @var Filesystem
     */
    protected $filesystem;


    public function __construct(
        TranslationsAdapterInterface $translationsAdapter,
        ResolverInterface $pathnameResolver
    ){
        $this->translationsAdapter = $translationsAdapter;
        $this->pathnameResolver = $pathnameResolver;
    }


    /**
     * @return TranslationsAdapterInterface
     */
    public function getTranslationsAdapter()
    {
        return $this->translationsAdapter;
    }


    public function scanFiles(array $importPaths, array $importFileExtensions)
    {
        $pathnameList = $this->pathnameResolver->resolve($importPaths, $importFileExtensions);

        foreach ($pathnameList as $pathname) {
            $this->translationsAdapter->addFromSource($pathname);
        }

        return $this;
    }


    public function exportToPoFile($exportPath, $filename = 'messages')
    {
        $pathname = $this->resolvePathname($exportPath, $filename, 'po');
        return $this->translationsAdapter->exportToPoFile($pathname);
    }


    public function exportToMoFile($exportPath, $filename = 'messages')
    {
        $pathname = $this->resolvePathname($exportPath, $filename, 'mo');
        return $this->translationsAdapter->exportToMoFile($pathname);
    }


    protected function resolvePathname($exportPath, $filename, $extension)
    {
        $exportPath = rtrim($exportPath, '\\/');
        $language = $this->translationsAdapter->getLanguage();

        $path = sprintf('%s/%s/LC_MESSAGES', $exportPath, $language);
        $filename = sprintf('%s.%s', $filename, $extension);

        $this->filesystem($path)->put($filename, '');

        return sprintf('%s/%s', $path, $filename);
    }


    protected function filesystem($path = '')
    {
        if ($path) {
            if (null === $this->filesystem) {
                $adapter = new FlySystemAdapter(['adapter' => new Local($path)]);
                $this->filesystem = new Filesystem($adapter);
            }
            else {
                $this->filesystem->getAdapter()->getFilesystem()->setPathPrefix($path);
            }
        }

        return $this->filesystem;
    }
}