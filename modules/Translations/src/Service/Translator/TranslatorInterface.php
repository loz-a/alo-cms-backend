<?php
namespace Translations\Service\Translator;


interface TranslatorInterface
{
    public function scanFiles(array $importPaths, array $importFileExtensions);

    public function exportToPoFile($exportPath, $filename = 'messages');

    public function exportToMoFile($exportPath, $filename = 'messages');
}