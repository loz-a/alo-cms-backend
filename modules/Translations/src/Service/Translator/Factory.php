<?php
namespace Translations\Service\Translator;

use Interop\Container\ContainerInterface;
use Translations\Service\Translator\TranslationsAdapter\GettextTranslations as GettextTranslationsAdapter;
use Translations\Service\PathnameResolver\Resolver as PathnameResolver;

class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        $translatorAdapter = new GettextTranslationsAdapter();
        $pathnameResolver = new PathnameResolver();

        return new Translator($translatorAdapter, $pathnameResolver);
    }
}