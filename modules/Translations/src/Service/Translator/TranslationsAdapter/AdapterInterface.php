<?php
namespace Translations\Service\Translator\TranslationsAdapter;


interface AdapterInterface
{
    /**
     * @param $domain
     * @return $this
     */
    public function setDomain($domain);

    public function getDomain();

    /**
     * @param $language
     * @return $this
     */
    public function setLanguage($language);

    public function getLanguage();

    /**
     * @param $count
     * @param $rule
     * @return $this
     */
    public function setPluralForms($count, $rule);

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setHeader($name, $value);

    public function addFromSource($pathname, array $options = []);

    public function exportToPoFile($exportPath);

    public function exportToMoFile($exportPath);
}