<?php
namespace Translations\Service\Translator\TranslationsAdapter;

use Gettext\Translations;
use Gettext\Extractors\PhpCode;

class GettextTranslations implements AdapterInterface
{
    /**
     * @var Translations
     */
    protected $translations;

    /**
     * @var array
     */
    protected $defaultExtractorOptions = [
        'extractComments' => false,

        'constants' => [],

        'functions' => [
            'gettext' => 'gettext',
            '__' => 'gettext',
            'translate' => 'gettext',
            'ngettext' => 'ngettext',
            'n__' => 'ngettext',
            'pgettext' => 'pgettext',
            'p__' => 'pgettext',
            'dgettext' => 'dgettext',
            'd__' => 'dgettext',
            'dpgettext' => 'dpgettext',
            'dp__' => 'dpgettext',
            'npgettext' => 'npgettext',
            'np__' => 'npgettext',
            'dnpgettext' => 'dnpgettext',
            'dnp__' => 'dnpgettext',
            'noop' => 'noop',
            'noop__' => 'noop',
        ]
    ];

    public function __construct()
    {
        $this->translations = new Translations();
    }


    public function setDomain($domain)
    {
        $this->translations->setDomain($domain);
        return $this;
    }


    public function getDomain()
    {
        return $this->translations->getDomain();
    }


    public function setLanguage($language)
    {
        $this->translations->setLanguage($language);
        return $this;
    }


    public function getLanguage()
    {
        return $this->translations->getLanguage();
    }


    public function setPluralForms($count, $rule)
    {
        $this->translations->setPluralForms($count, $rule);
        return $this;
    }


    public function setHeader($name, $value)
    {
        $this->translations->setHeader($name, $value);
        return $this;
    }


    public function addFromSource($pathname, array $options = [])
    {
        $options = count($options)
            ? array_merge($this->defaultExtractorOptions, $options)
            : $this->defaultExtractorOptions;

        $this->translations->addFromPhpCodeFile($pathname, $options);
    }


    public function exportToPoFile($pathname)
    {
        return $this->translations->toPoFile($pathname);
    }


    public function exportToMoFile($pathname)
    {
        return $this->translations->toMoFile($pathname);
    }

}