<?php
namespace Translations\Service\PathnameResolver;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class Resolver implements ResolverInterface
{
    public function resolve(array $paths = [], array $extensions = [])
    {
        if (!count($paths)) {
            return null;
        }

        $regExp = count($extensions) ? sprintf('/\.(?:%s)$/', implode('|', $extensions)) : '';

        // get pathname list for module which translating
        $pathnameList = [];
        foreach ($paths as $path) {
            $pathnameList = array_merge($pathnameList, $this->resolveFiles($path, $regExp));
        }

        // get pathname list for modules config files
        $pathnameList = array_merge(
            $pathnameList,
            $this->resolveFiles(getcwd().'/modules', '/module\.config\.php$/')
        );

        // normalize each pathname
        $pathnameList = array_map(function($pathname) {
//            $pathname = str_replace(['\\', '/'], DIRECTORY_SEPARATOR, trim($pathname));
            return realpath(trim($pathname));
        }, $pathnameList);

        // get unique list
        return array_keys(array_flip($pathnameList));
    }


    protected function resolveFiles($path, $regExp = '')
    {
        $dir = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::SELF_FIRST);

        if ($regExp) {
            $files = new RegexIterator($files, $regExp);
        }

        $pathnameList = [];
        foreach ($files as $file) {
            $pathnameList[] = $file->getPathname();
        }

        return $pathnameList;
    }
}