<?php
namespace Translations\Service\PathnameResolver;



interface ResolverInterface
{
    public function resolve(array $paths = [], array $extensions = []);
}