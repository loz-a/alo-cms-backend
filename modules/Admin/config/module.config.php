<?php

use Admin\ConfigProvider;

return [
    'dependencies' => [
        'factories' => [
            Admin\Action\Index::class => Admin\Action\IndexFactory::class,
            Admin\Action\Config::class => Admin\Action\ConfigFactory::class,
        ]
    ],


    'routes' => [
        [
            'name' => 'admin',
            'path' => '/admin',
            'middleware' => Admin\Action\Index::class,
            'allowed_methods' => ['GET']
        ],
        [
            'name' => 'admin-config',
            'path' => '/admin-config.json',
            'middleware' => Admin\Action\Config::class,
            'allowed_methods' => ['GET']
        ]
    ],


    'templates' => [
        'map' => [
            'layout::admin' => __DIR__ . '/../templates/layout/admin.phtml',
            'admin::index' => __DIR__ . '/../templates/admin/index.phtml'
        ],
        'paths' => [
            'admin'    => [__DIR__ . '/../templates/admin'],
            'layout' => [__DIR__ . '/../templates/layout'],
        ],
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../data/languages',
                'pattern' => '%s.mo',
                'text_domain' => ConfigProvider::MODULE_NAME
            ]
        ]
    ],

    'admin_navigation' => [
        'admin' => [
            'title'=> translate('Admin dashboard'),
            'icon' => 'dashboard',
            'index' => 1
        ]
    ]
];