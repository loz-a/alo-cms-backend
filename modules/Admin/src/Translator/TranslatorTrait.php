<?php
namespace Admin\Translator;

use Admin\ConfigProvider;
use App\Translator\AppTranslatorTrait;


trait TranslatorTrait
{
    use AppTranslatorTrait;

    protected function getTranslatorTextDomain()
    {
        return ConfigProvider::MODULE_NAME;
    }

}