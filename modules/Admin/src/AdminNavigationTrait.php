<?php
namespace Admin;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Helper\UrlHelper;
use Zend\Stdlib\SplPriorityQueue;

trait AdminNavigationTrait
{
    protected function getAdminNavigation(ContainerInterface $container)
    {
        $config = $container->get('config');
        $urlHelper = $container->get(UrlHelper::class);

        $adminNavigation = $config['admin_navigation'] ?? [];

        $module = [];
        $modules = new SplPriorityQueue();
        $modules->setExtractFlags(SplPriorityQueue::EXTR_DATA);


        foreach ($adminNavigation as $key => $params) {
            $params['link'] = $urlHelper->generate($key);
            $params['routeName'] = $key;

            if ($key === 'home' || $key === 'logout') {
                $module[] = $params;
            } else {
                $modules->insert($params, $params['index'] ?? 0);
            }

        }

        return [
            'modules' => array_reverse($modules->toArray()),
            'module'  => $module
        ];
    }
}