<?php
namespace Admin\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Admin\Responder\Index as IndexResponder;

/**
 * @Api.TokenVerify
 */
class Index
{
    /**
     * @var IndexResponder
     */
    private $responder;

    public function __construct(IndexResponder $responder)
    {
        $this->responder = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        return $this->responder->__invoke();
    } // __invoke()
}