<?php
namespace Admin\Action;

use Admin\AdminNavigationTrait;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;
use App\Responder\JsonResponder;

class ConfigFactory
{
    use AdminNavigationTrait;

    public function __invoke(ContainerInterface $container)
    {

        $translator = $container->get(TranslatorInterface::class);
        $responder  = new JsonResponder();

        return new Config(
            $translator,
            $this->getAdminNavigation($container),
            $responder
        );
    } // __invoke()



}