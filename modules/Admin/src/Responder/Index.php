<?php
namespace Admin\Responder;

use App\Responder\HtmlResponder;

class Index extends HtmlResponder
{
    public function __invoke()
    {
        $data = [
            'layout'   => 'layout::admin',
            'template' => 'admin::index'
        ];

        return $this->getResponse($data);
    } // __invoke()
}