<?php
namespace Languages\Options;

interface LanguageValidatorInterface
{
    public function setLanguageMaxLength($maxLength);

    public function getLanguageMaxLength();


    public function setLanguageDescriptionMaxLength($maxLength);

    public function getLanguageDescriptionMaxLength();
}