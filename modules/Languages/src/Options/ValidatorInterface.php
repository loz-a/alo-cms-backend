<?php
namespace Languages\Options;

interface ValidatorInterface
    extends LanguageValidatorInterface,
            LocaleValidatorInterface,
            SupportedLocalesInterface
{

}