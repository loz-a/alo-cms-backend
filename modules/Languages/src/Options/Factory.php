<?php
namespace Languages\Options;

use Interop\Container\ContainerInterface;

class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $options = $config['languages'] ?? [];
        $options['supported_locales'] = $config['supported_locales'] ?? [];

        return new ModuleOptions($options);
    }
}