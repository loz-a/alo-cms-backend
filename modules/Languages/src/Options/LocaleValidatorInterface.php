<?php
namespace Languages\Options;

interface LocaleValidatorInterface
{
    public function setLocaleMaxLength($maxLength);

    public function getLocaleMaxLength();
}