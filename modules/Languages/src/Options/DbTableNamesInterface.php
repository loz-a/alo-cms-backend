<?php
namespace Languages\Options;

interface DbTableNamesInterface
{
    public function setLanguagesTableName($tableName);

    public function getLanguagesTableName();


    public function setLanguagesTranslationsTableName($tableName);

    public function getLanguagesTranslationsTableName();


    public function setLocalesTableName($tableName);

    public function getLocalesTableName();
}