<?php
namespace Languages\Options;

interface SupportedLocalesInterface
{
    public function setSupportedLocales(array $supportedLocales);

    public function getSupportedLocales();
}