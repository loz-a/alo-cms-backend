<?php
namespace Languages\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements DbTableNamesInterface, ValidatorInterface
{
    protected $languagesTableName = 'languages';

    public function setLanguagesTableName($tableName)
    {
        $this->languagesTableName = $tableName;
    }

    public function getLanguagesTableName()
    {
        return $this->languagesTableName;
    }


    protected $languagesTranslationsTableName = 'languages_translations';

    public function setLanguagesTranslationsTableName($tableName)
    {
        $this->languagesTranslationsTableName = $tableName;
    }

    public function getLanguagesTranslationsTableName()
    {
        return $this->languagesTranslationsTableName;
    }


    protected $localesTableName = 'locales';

    public function setLocalesTableName($tableName)
    {
        $this->localesTableName = $tableName;
    }

    public function getLocalesTableName()
    {
        return $this->localesTableName;
    }


    protected $languageMaxLength = 3;

    public function setLanguageMaxLength($maxLength)
    {
        $this->languageMaxLength = $maxLength;
    }

    public function getLanguageMaxLength()
    {
        return $this->languageMaxLength;
    }


    protected $languageDescriptionMaxLength = 255;

    public function setLanguageDescriptionMaxLength($maxLength)
    {
        $this->languageDescriptionMaxLength = $maxLength;
    }

    public function getLanguageDescriptionMaxLength()
    {
        return $this->languageDescriptionMaxLength;
    }


    protected $localeMaxLength = 7;

    public function setLocaleMaxLength($maxLength)
    {
        $this->localeMaxLength = $maxLength;
    }

    public function getLocaleMaxLength()
    {
        return $this->localeMaxLength;
    }


    protected $supportedLocales = [];

    public function setSupportedLocales(array $supportedLocales)
    {
        $this->supportedLocales = $supportedLocales;
    }

    public function getSupportedLocales()
    {
        return $this->supportedLocales;
    }


}