<?php
namespace Languages\Action;

use Interop\Container\ContainerInterface;
use Languages\Domain\Service\SupportedLocalesInterface as SupportedLocalesServiceInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
// use Zend\Diactoros\Response\JsonResponse;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;
use Languages\Responder\Index as IndexResponder;

class IndexFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $supportedLocalesService = $container->get(SupportedLocalesServiceInterface::class);
        $languagesService = $container->get(LanguagesServiceInterface::class);

        $template = $container->get(TemplateRendererInterface::class);
        $responder = new IndexResponder($template);

    //     $data = [
    //       'description' => "Basque (Spain)",
    //       'displayValues' => [
    //             "rJJt4AN_g" => [
    //                 'id' => "rJJt4AN_g",
    //                 'locale' => ['id' => "85", 'value' => "ar_SD"],
    //                 'value' => "test 1"
    //             ],
    //             "rJJt4AN_q" => [
    //                 'id' => "rJJt4AN_q",
    //                 'locale' => ['id' => "84", 'value' => "ar_BH"],
    //                 'value' => "test 2"
    //             ],
    //             "rJJt4AN_1" => [
    //                 'id' => "rJJt4AN_1",
    //                 'locale' => ['id' => "84", 'value' => "ar_BH"],
    //                 'value' => "test 2"
    //             ]
    //         ],
    //       'id' => "75",
    //       'is_enable' => "1",
    //       'language' => "eu",
    //       'locale' => ['id' => "86", 'value' => "eu_ES"]
    //   ];
      //
    //     $inputFilter = $container->get(\Languages\Domain\InputFilter\UpdateLanguage::class);
    //     $inputFilter->setData($data);
      //
    //     echo '<pre>' . var_export($inputFilter->isValid(), true)  . '</pre><br />';
    //     echo '<pre>' . var_export($inputFilter->getValues(), true) . '</pre><br />';
    //     echo '<pre>' . var_export($inputFilter->getMessages(), true) . '</pre>';
    //     die;

        /**
         * @var $storage \Languages\Domain\Storage\Db\Languages
         */
//        $storage = $container->get(\Languages\Domain\Storage\LanguagesInterface::class);
//
//
//
//        $result = $storage->findById(12);
//        var_dump(__FILE__ . '. Line: ' . __LINE__);var_dump(iterator_to_array($result));die;

        /**
         * @var \Languages\Domain\Repository\LanguagesInterface $languagesRepository
         */
//        $languagesRepository = $container->get(\Languages\Domain\Repository\LanguagesInterface::class);
//
//        $result = $languagesRepository->disableById(12);

        /**
         * @var \Languages\Domain\Service\LanguagesInterface $langService
         */
      //  $langService = $container->get(\Languages\Domain\Service\LanguagesInterface::class);
      //   echo '<pre>' . var_export($langService->getAll(), true)  . '</pre><br />';
//        $result = $langService->enableLanguage(12);

//        $language = new \Languages\Domain\Entity\Language();
//        $language
//            ->setLocale(
//                (new \Languages\Domain\Entity\Locale())->setValue('br_FR')
//            )
//            ->setValue('br')
//            ->setDescription('Breton (France)')
//            ->setIsEnable(false)
//            ->getDisplayValues()
//            ->add(
//                (new \Languages\Domain\Entity\LanguageDisplayValue())
//                    ->setValue('BR')
//                    ->setLocale(
//                        (new \Languages\Domain\Entity\Locale())->setId(8)->setValue('en_US')
//                    )
//            )
//            ->add(
//                (new \Languages\Domain\Entity\LanguageDisplayValue())
//                    ->setValue('БР')
//                    ->setLocale(
//                        (new \Languages\Domain\Entity\Locale())->setId(10)->setValue('uk_UA')
//                    )
//            );
//
//        $result = $languagesRepository->create($language);


//        $language = new \Languages\Domain\Entity\Language();
//        $language
//            ->setId(15)
//            ->setDescription('Ukrainian (Ukraine)')
//            ->setIsEnable(true)
//            ->getDisplayValues()
//            ->add(
//                (new \Languages\Domain\Entity\LanguageDisplayValue())
//                    ->setId(14)
//                    ->setValue('UKR11')
//                    ->setLocale(
//                        (new \Languages\Domain\Entity\Locale())->setId(8)->setValue('en_US')
//                    )
//            )
//            ->add(
//                (new \Languages\Domain\Entity\LanguageDisplayValue())
//                    ->setId(9)
//                    ->setValue('УКР')
//                    ->setLocale(
//                        (new \Languages\Domain\Entity\Locale())->setId(10)->setValue('uk_UA')
//                    )
//            );
//
//
//        $result = $languagesRepository->update($language);
//        var_dump(__FILE__ . '. Line: ' . __LINE__);var_dump($result);die;

//        $result = $languagesRepository->removeById(44);

//        $result = $languagesRepository->findByLocale('uk_UA');
        //$result = $languagesRepository->findOneByIdAndTranslationLocale(15, 'uk_UA');


//        $result = iterator_to_array($languagesRepository->fetchAll())[0]->getDisplayValues()[0]->getLocale()->getValue();
//        $result = $languagesRepository->findById(12);
//        $result = iterator_to_array($result);
//        var_dump(__FILE__ . '. Line: ' . __LINE__);var_dump($result);die;

        /**
         * @var \Languages\Domain\InputFilter\Language $inputFilter
         */
//        $inputFilter = $container->get(\Languages\Domain\InputFilter\Language::class);
//
//        $data = [
//            'locale' => 'ur_PK',
//            'language' => 'ur',
//            'description' => 'Urdu (Pakistan)',
//            'is_enable' => 'sfdlksdjfl'
//        ];
//
//        $inputFilter->setData($data);
//        $result = $inputFilter->isValid();
//        var_dump($result); die;
//        var_dump($inputFilter->getMessages());
//        var_dump($inputFilter->getValues());
//die;




        return new Index(
            $supportedLocalesService,
            $languagesService,
            $responder
        );
    }
}
