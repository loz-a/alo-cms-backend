<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @Api.TokenVerify
 */
class DeleteLanguage
{
    use ErrorResponseTrait;

    /**
     * @var LanguagesServiceInterface
     */
    private $languagesService;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(
        LanguagesServiceInterface $languagesService,
        JsonResponder $responder
    ){
        $this->languagesService = $languagesService;
        $this->responder = $responder;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        $params = $request->getParsedBody();

        if (!isset($params['languageId'])) {
            return $this->errorResponse('Language id is not specified');
        }

        return $this->responder->getResponse([
            'data' => [
                'language' => $this->languagesService->deleteById($params['languageId'])
            ]
        ]);
    }


    public function getResponder(): JsonResponder
    {
        return $this->responder;
    }
}