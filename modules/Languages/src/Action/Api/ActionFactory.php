<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Interop\Container\ContainerInterface;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;

class ActionFactory 
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $languagesService = $container->get(LanguagesServiceInterface::class);
        $responder = new JsonResponder();

        return new $requestedName($languagesService, $responder);
    }
}