<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;
use Languages\Domain\InputFilter\AbstractLanguage as LanguageInputFilter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @Api.TokenVerify
 */
class CreateLanguage
{
    /**
     * @var LanguagesServiceInterface
     */
    private $languagesService;

    /**
     * @var LanguageInputFilter
     */
    private $languageInputFilter;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(
        LanguagesServiceInterface $languagesService,
        LanguageInputFilter $languageInputFilter,
        JsonResponder $responder
    ){
        $this->languagesService = $languagesService;
        $this->languageInputFilter = $languageInputFilter;
        $this->responder = $responder;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        $params = $request->getParsedBody();
        $inputFilter = $this->languageInputFilter;

        $inputFilter->setData($params);

        $responseData = [];
        if (!$inputFilter->isValid()) {
            $responseData['data']['validateErrors'] = $inputFilter->getMessages();
        }
        else {
            $data = $inputFilter->getValues();                         
            $responseData['data']['language'] = $this->languagesService->save($data);
        }

        return $this->responder->getResponse($responseData);
    }

}
