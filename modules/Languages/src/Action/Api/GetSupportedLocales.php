<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @Api.TokenVerify
 */
class GetSupportedLocales
{
    /**
     * @var array
     */
    private $supprotedLocales;
    /**
     * @var JsonResponder
     */
    private $responder;


    public function __construct(
        array $supportedLocales,
        JsonResponder $responder
    ){
        $this->supprotedLocales = $supportedLocales;
        $this->responder = $responder;
    }


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        return $this->responder->getResponse([
            'data' => [
                'locales' => $this->supprotedLocales
            ]
        ]);
    }
}