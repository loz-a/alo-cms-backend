<?php
namespace Languages\Action\Api;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class UpdateLanguage extends CreateLanguage
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        $params = $request->getParsedBody();
        if (isset($params['id'])) {
            return parent::__invoke($request, $response);
        }

        return $next($request, $response);
    }
}
