<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Interop\Container\ContainerInterface;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;
use Languages\Domain\InputFilter\{
    CreateLanguage as CreateLanguageInputFilter,
    UpdateLanguage as UpdateLanguageInputFilter
};

class SaveLanguageFactory
{
    public function __invoke(ContainerInterface $container, $requestedName)
    {
        $languagesService = $container->get(LanguagesServiceInterface::class);

        $inputFilter = null;
        if ($requestedName === CreateLanguage::class) {
            $inputFilter = $container->get(CreateLanguageInputFilter::class);
        }
        else if ($requestedName === UpdateLanguage::class) {
            $inputFilter = $container->get(UpdateLanguageInputFilter::class);
        }

        $responder = new JsonResponder();

        return new $requestedName($languagesService, $inputFilter, $responder);
    }
}
