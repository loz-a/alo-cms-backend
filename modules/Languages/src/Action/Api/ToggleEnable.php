<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Languages\Domain\Service\LanguagesInterface as LanguageServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @Api.TokenVerify
 */
class ToggleEnable
{
    use ErrorResponseTrait;

    /**
     * @var LanguageServiceInterface
     */
    private $languagesService;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(
        LanguageServiceInterface $languagesService,
        JsonResponder $responder
    ){
        $this->languagesService = $languagesService;
        $this->responder = $responder;
    }


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        $params = $request->getParsedBody();

        if (!isset($params['languageId'])) {
            return $this->errorResponse('Language id not specified');
        }

        if (!isset($params['isEnable'])) {
            return $this->errorResponse('Language availability not specified');
        }
        $languageId = $params['languageId'];
        $isEnable = filter_var($params['isEnable'], FILTER_VALIDATE_BOOLEAN);
        $language = null;

        $method = sprintf('%sLanguage', $isEnable ? 'enable' : 'disable');

        return $this->responder->getResponse([
            'data' => [
                'language' => $this->languagesService->{$method}($languageId)
            ]
        ]);
    }


    public function getResponder(): JsonResponder
    {
        return $this->responder;
    }
}