<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;
use Interop\Container\ContainerInterface;

class GetSupportedLocalesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $supportedLocales = $config['supported_locales'];

        return new GetSupportedLocales(
            $supportedLocales,
            new JsonResponder()
        );
    }
}