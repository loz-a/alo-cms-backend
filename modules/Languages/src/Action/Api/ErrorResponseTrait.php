<?php
namespace Languages\Action\Api;

use App\Responder\JsonResponder;

trait ErrorResponseTrait
{
    protected function errorResponse($msg)
    {
        return $this->getResponder()->getResponse([
            'response_status' => 500,
            'data' => [
                'error' => $msg
            ]
        ]);
    }


    abstract public function getResponder(): JsonResponder;
}