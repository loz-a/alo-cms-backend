<?php
namespace Languages\Action\Api;

use Interop\Container\ContainerInterface;
use Languages\Domain\Service\SupportedLocalesInterface as SupportedLocalesServiceInterface;
use App\Responder\JsonResponder;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;

class GetInitStateFactory
{
    public function __invoke(ContainerInterface $container)
    {

        $supportedLocalesService = $container->get(SupportedLocalesServiceInterface::class);
        $languagesService = $container->get(LanguagesServiceInterface::class);

        return new GetInitState(
            $supportedLocalesService,
            $languagesService,
            new JsonResponder()
        );
    }
}