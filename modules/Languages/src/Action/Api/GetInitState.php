<?php
namespace Languages\Action\Api;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Languages\Domain\Service\SupportedLocalesInterface as SupportedLocalesServiceInterface;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;
use App\Responder\JsonResponder;

/**
 * @Api.TokenVerify
 */
class GetInitState
{
    /**
     * @var SupportedLocalesServiceInterface
     */
    private $supportedLocalesService;

    /**
     * @var LanguagesServiceInterface
     */
    private $languagesService;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(
        SupportedLocalesServiceInterface $supportedLocalesService,
        LanguagesServiceInterface $languagesService,
        JsonResponder $responder
    ){
        $this->supportedLocalesService = $supportedLocalesService;
        $this->languagesService = $languagesService;
        $this->responder = $responder;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){

        $supportedLocales = $this->supportedLocalesService->getAll();
        $languages = $this->languagesService->getAll();

        return $this->responder->getResponse([
            'data' => [
                'initState' => [
                    'languages' => $languages,
                    'supportedLocales' => $supportedLocales
                ]
            ]
        ]);
    }

}