<?php
namespace Languages\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Languages\Domain\Service\SupportedLocalesInterface as SupportedLocalesServiceInterface;
use Languages\Domain\Service\LanguagesInterface as LanguagesServiceInterface;
use Languages\Responder\Index as IndexResponder;

///**
// * @Api.TokenVerify
// */
class Index
{
    /**
     * @var SupportedLocalesServiceInterface
     */
    private $supportedLocalesService;

    /**
     * @var LanguagesServiceInterface
     */
    private $languagesService;

    /**
     * @var IndexResponder
     */
    private $responder;

    public function __construct(
        SupportedLocalesServiceInterface $supportedLocalesService,
        LanguagesServiceInterface $languagesService,
        IndexResponder $responder
    ){
        $this->supportedLocalesService = $supportedLocalesService;
        $this->languagesService = $languagesService;
        $this->responder = $responder;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){

        $supportedLocales = $this->supportedLocalesService->getAll();
        $languages = $this->languagesService->getAll();

        return $this->responder->__invoke($supportedLocales, $languages);
    }

}