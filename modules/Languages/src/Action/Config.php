<?php
namespace Languages\Action;

use App\Responder\JsonResponder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Languages\Translator\TranslatorTrait;
use Zend\I18n\Translator\Translator;

/**
 * @Api.TokenVerify
 */
class Config
{
    use TranslatorTrait;

    /**
     * @var array
     */
    private $navigation;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(Translator $translator, array $navigation, JsonResponder $responder)
    {
        $this->translator = $translator;
        $this->navigation = $navigation;
        $this->responder = $responder;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ){
        return $this->responder->getResponse([
            'data' => [
                'translations' => $this->getTranslateMessages(),
                'navigation' => $this->navigation
            ]
        ]);
    }
}