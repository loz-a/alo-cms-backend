<?php
namespace Languages\Responder;

use App\Responder\HtmlResponder;

class Index extends HtmlResponder
{
    public function __invoke(array $supportedLocales, array $languages)
    {

        $data = [
            'template' => 'languages::index',
            'layout' => 'layout::admin',
            'template_params' => [
                'initState' => json_encode([
                    'languages' => $languages,
                    'supportedLocales' => $supportedLocales
                ])
            ]
        ];

        return $this->getResponse($data);
    }
}