<?php
namespace Languages\Translator;

use App\Translator\AppTranslatorTrait;
use Languages\ConfigProvider;

trait TranslatorTrait
{
    use AppTranslatorTrait;

    protected function getTranslatorTextDomain()
    {
        return  ConfigProvider::MODULE_NAME;
    }
}