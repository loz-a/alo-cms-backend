<?php
namespace Languages\Domain\Repository;

use Languages\Domain\Storage\Db\ResultSet\SupportedLocalesHydratingResultSet;

class SupportedLocales implements SupportedLocalesInterface
{
    /**
     * @var SupportedLocalesHydratingResultSet
     */
    protected $hydratingResultSet;

    /**
     * @var  array
     */
    protected $data;


    public function __construct(
        array $data,
        SupportedLocalesHydratingResultSet $hydratingResultSet
    ){
        $this->data = $data;
        $this->hydratingResultSet = $hydratingResultSet;
    }

    public function fetchAll()
    {
        return $this->hydratingResultSet->initialize($this->data)->toArray();
    }


    public function has(string $locale) {
        return array_key_exists($locale, $this->data);
    }

}