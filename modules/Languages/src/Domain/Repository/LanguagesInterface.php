<?php
namespace Languages\Domain\Repository;

use Languages\Domain\Entity\LanguageInterface;

interface LanguagesInterface
{
    /**
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function fetchAll();

//    public function findByLocale($locale);

    public function findById($id);

//    public function findOneByIdAndTranslationLocale($id, $locale);

    public function removeById($id);

    public function create(LanguageInterface $language);

    public function update(LanguageInterface $language);


    /**
     * @param $id
     * @return LanguageInterface
     */
    public function enableById($id);

    /**
     * @param $id
     * @return LanguageInterface
     */
    public function disableById($id);

    public function lookupByLanguage($language);

    public function lookupByLocale($locale);
}