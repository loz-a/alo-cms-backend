<?php
namespace Languages\Domain\Repository;

interface SupportedLocalesInterface
{
    public function fetchAll();

    public function has(string $locale);
}