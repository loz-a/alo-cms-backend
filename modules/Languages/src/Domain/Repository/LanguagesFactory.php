<?php
namespace Languages\Domain\Repository;

use Interop\Container\ContainerInterface;
use Languages\Domain\Storage\LanguagesInterface as LanguagesStorageInterface;
use Languages\Domain\Storage\Db\ResultSet\LanguageHydratingResultSet;

class LanguagesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $storage = $container->get(LanguagesStorageInterface::class);
        $hydratingResultSet = $container->get(LanguageHydratingResultSet::class);
        return new Languages($storage, $hydratingResultSet);
    }
}