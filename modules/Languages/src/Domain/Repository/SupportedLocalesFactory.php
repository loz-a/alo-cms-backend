<?php
namespace Languages\Domain\Repository;

use Interop\Container\ContainerInterface;
use Languages\Domain\Storage\Db\ResultSet\SupportedLocalesHydratingResultSet;

class SupportedLocalesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $supportedLocales = $config['supported_locales'] ?? [];
        $hydratingResultSet = $container->get(SupportedLocalesHydratingResultSet::class);

        return new SupportedLocales($supportedLocales, $hydratingResultSet);
    }
}