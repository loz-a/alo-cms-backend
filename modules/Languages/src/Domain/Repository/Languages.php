<?php
namespace Languages\Domain\Repository;

use Languages\Domain\Entity\LanguageInterface;
use Languages\Domain\Storage\Db\ResultSet\LanguageHydratingResultSet;
use Languages\Domain\Storage\LanguagesInterface as LanguagesStorageInterface;

class Languages implements LanguagesInterface
{
    /**
     * @var LanguagesStorageInterface
     */
    protected $storage;

    /**
     * @var LanguageHydratingResultSet
     */
    protected $hydratingResultSet;


    public function __construct(
        LanguagesStorageInterface $storage,
        LanguageHydratingResultSet $hydratingResultSet
    ){
        $this->storage = $storage;
        $this->hydratingResultSet = $hydratingResultSet;
    }


    public function fetchAll()
    {
        $result = $this->storage->fetchAll();
        return $this->hydratingData($result)->toArray();
    }


//    public function findByLocale($locale)
//    {
//        $result = $this->storage->findByLocale($locale);
//        return $this->hydratingResultSet($result, new LanguageHydratorAggregate());
//    }


    public function findById($id)
    {
        $result = $this->storage->findById($id);

        if (null !== $result) {
            $resultSet = $this->hydratingData($result);
            $result = $resultSet->current();
        }

        return $result;
    }


//    public function findOneByIdAndTranslationLocale($id, $locale)
//    {
//        $result = $this->storage->findOneByIdAndTranslationLocale($id, $locale);
//
//        if (null !== $result) {
//            $result = $this->hydratingResult($result, new LanguageHydratorAggregate());
//        }
//
//        return $result;
//    }


    public function removeById($id)
    {
        $language = $this->findById($id);

        if (null !== $language) {
            $extractedData = $this->extractData($language);

            if (null === $this->storage->remove($extractedData)) {
                return null;
            }
        }

        return $language;
    }


    public function create(LanguageInterface $language)
    {
        $extractedData = $this->extractData($language);
        $createdId = $this->storage->create($extractedData);

        return $this->findById($createdId);
    }


    public function update(LanguageInterface $language)
    {
        $extractedData = $this->extractData($language);        
        $updatedId = $this->storage->update($extractedData);

        return $this->findById($updatedId);
    }


    public function enableById($id)
    {
        $result = $this->storage->enableById($id);
        return $this->hydratingData($result)->current();
    }


    public function disableById($id)
    {
        $result = $this->storage->disableById($id);
        return $this->hydratingData($result)->current();
    }


    public function lookupByLanguage($language)
    {
        return $this->storage->lookupByLanguage($language);
    }


    public function lookupByLocale($locale)
    {
        return $this->storage->lookupByLocale($locale);
    }


    /**
     * @param $data
     *
     * @return LanguageHydratingResultSet
     */
    protected function hydratingData($data)
    {
        return $this->hydratingResultSet->initialize($data);
    }


    /**
     * @param $object
     *
     * @return array
     */
    protected function extractData($object)
    {
        return $this->hydratingResultSet->getHydrator()->extract($object);
    }
}
