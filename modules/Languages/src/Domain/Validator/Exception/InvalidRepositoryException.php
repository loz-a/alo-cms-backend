<?php
namespace Languages\Domain\Validator\Exception;

class InvalidRepositoryException extends \InvalidArgumentException
    implements ExceptionInterface
{

}