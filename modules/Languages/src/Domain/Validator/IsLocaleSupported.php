<?php
namespace Languages\Domain\Validator;

use Languages\Domain\Repository\SupportedLocalesInterface;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class IsLocaleSupported extends AbstractValidator
{
    const ERROR_LOCALE_NOT_SUPPORTED = 'localeNotSupported';

    protected $messageTemplates = [
        self::ERROR_LOCALE_NOT_SUPPORTED => "Locale '%value%' not supported"
    ];

    /**
     * @var SupportedLocalesInterface
     */
    protected $repository;


    public function __construct(
        SupportedLocalesInterface $repository,
        $options = null
    ){
        parent::__construct($options);

        $this->repository = $repository;
    }

    public function isValid($value)
    {
        $this->setValue($value);

        list($language, $territory) = explode('_', $value);
        $normalizedLocale = sprintf('%s_%s', strtolower($language), strtoupper($territory));

        if (!$this->repository->has($normalizedLocale)) {
            $this->error(self::ERROR_LOCALE_NOT_SUPPORTED);
            return false;
        }
        return true;
    }
}