<?php
namespace Languages\Domain\Validator;

use Components\Validator\AbstractRecord;
use Components\Validator\Exception\InvalidKeyException;
use Languages\Domain\Repository\LanguagesInterface as LanguagesRepositoryInterface;
use Languages\Domain\Validator\Exception\InvalidRepositoryException;

class NoRecordExists extends AbstractRecord
{
    public function isValid($value)
    {
        $this -> setValue($value);
        $result = $this->query($value);

        if ($result) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    }


    protected function query($value)
    {
        $result = false;

        switch ($this->key) {
            case 'locale':
                $result = $this->repository->lookupByLocale($value);
                break;
//            case 'language':
//                $result = $this->repository->lookupByLanguage($value);
//                break;
            default:
                throw new InvalidKeyException(sprintf('Invalid key - %s'), $this->key);
        }
        return $result;
    }


    public function setRepository($repository)
    {
        if (!$repository instanceof LanguagesRepositoryInterface) {
            throw new InvalidRepositoryException(
                sprintf('Invalid repository type. %s is expected', LanguagesRepositoryInterface::class)
            );
        }

        $this->repository = $repository;
        return $this;
    }


    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    public function setExclude($exclude)
    {
        $this->exclude = $exclude;
        return $this;
    }

}