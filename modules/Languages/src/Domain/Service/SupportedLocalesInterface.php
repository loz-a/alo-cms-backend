<?php
namespace Languages\Domain\Service;

interface SupportedLocalesInterface
{
    public function getAll();
}