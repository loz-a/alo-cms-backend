<?php
namespace Languages\Domain\Service;

use Interop\Container\ContainerInterface;
use Languages\Domain\Repository\LanguagesInterface as LanguagesRepositoryInterface;
use Languages\Domain\Hydrator\LanguageInterface as LanguageHydratorInterface;

class LanguagesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $languagesRepository = $container->get(LanguagesRepositoryInterface::class);
        $languageHydrator = $container->get(LanguageHydratorInterface::class);

        return new Languages($languagesRepository, $languageHydrator);
    }
}