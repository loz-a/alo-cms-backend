<?php
namespace Languages\Domain\Service;

use Languages\Domain\Repository\LanguagesInterface as LanguagesRepositoryInterface;
use Languages\Domain\Hydrator\LanguageInterface as LanguageHydratorInterface;

class Languages implements LanguagesInterface
{
    /**
     * @var LanguagesRepositoryInterface
     */
    protected $languagesRepository;

    /**
     * @var LanguageHydratorInterface
     */
    protected $languageHydrator;

    public function __construct(
        LanguagesRepositoryInterface $languagesRepository,
        LanguageHydratorInterface $languageHydrator
    ){
        $this->languagesRepository = $languagesRepository;
        $this->languageHydrator = $languageHydrator;
    }


    /**
     * @return array
     */
    public function getAll()
    {
        return $this->languagesRepository->fetchAll();
    }


    public function getById($id)
    {
        return $this->languagesRepository->findById($id);
    }


//    public function create(array $data)
//    {
//        $language = new LanguageEntity();
//
//        $language
//            ->setLocale(
//                (new LocaleEntity())->setLocale($data['locale'])
//            )
//            ->setLanguage($data['language'])
//            ->setDescription($data['description'])
//            ->setIsEnable($data['isEnable']);
//
//        $language = $this->languagesRepository->create($language);
//
//        if (null !== $language) {
//            $language = $this->dataDenormalizer->__invoke($language);
//        }
//
//        return $language;
//    }


    public function save(array $data)
    {
        $language = $this->languageHydrator->hydrateLanguage($data);
        $method = isset($data['id']) ? 'update' : 'create';        
        $result = $this->languagesRepository->{$method}($language);
        return $this->languageHydrator->extract($result);
    }


    public function deleteById($languageId)
    {
        $result = $this->languagesRepository->removeById($languageId);
        return $this->languageHydrator->extract($result);
    }


    public function disableLanguage($languageId)
    {
        return $this->toggleLanguage($languageId, false);
    }


    public function enableLanguage($languageId)
    {
        return $this->toggleLanguage($languageId, true);
    }


    protected function toggleLanguage($languageId, $flag)
    {
        $languageId = (int) $languageId;
        $method = $flag ? 'enableById' : 'disableById';
        $result = $this->languagesRepository->{$method}($languageId);
        return $this->languageHydrator->extract($result);
    }
}
