<?php
namespace Languages\Domain\Service;

use Interop\Container\ContainerInterface;
use Languages\Domain\Repository\SupportedLocalesInterface;

class SupportedLocalesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $repository = $container->get(SupportedLocalesInterface::class);
        return new SupportedLocales($repository);
    }
}