<?php
namespace Languages\Domain\Service;

use Languages\Domain\Repository\SupportedLocalesInterface as SupportedLocalesRepositoryInterface;

class SupportedLocales implements SupportedLocalesInterface
{
    /**
     * @var SupportedLocalesRepositoryInterface
     */
    protected $supportedLocalesRepository;

    public function __construct(
        SupportedLocalesRepositoryInterface $supportedLocalesRepository
    ){
        $this->supportedLocalesRepository = $supportedLocalesRepository;
    } // __construct()


    public function getAll()
    {
        return $this->supportedLocalesRepository->fetchAll();
    }

}