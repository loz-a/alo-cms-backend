<?php
namespace Languages\Domain\Service;


interface LanguagesInterface
{
    public function getById($id);

    public function getAll();

    public function save(array $data);

    public function deleteById($id);

    public function disableLanguage($languageId);

    public function enableLanguage($languageId);
}