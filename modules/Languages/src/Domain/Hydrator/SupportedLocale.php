<?php
namespace Languages\Domain\Hydrator;

use Languages\Domain\Entity\SupportedLocale as SupportedLocaleEntity;
use Zend\Hydrator\ClassMethods;

class SupportedLocale extends ClassMethods implements SupportedLocaleInterface
{
    public function hydrateLocale(array $data)
    {
        return $this->hydrate($data, new SupportedLocaleEntity());
    }

}