<?php
namespace Languages\Domain\Hydrator;

use Languages\Domain\Entity\Language as LanguageEntity;
use Languages\Domain\Entity\Locale as LocaleEntity;
use Languages\Domain\Entity\LocaleInterface as LocaleEntityInterface;
use Zend\Hydrator\ClassMethods;
use Zend\Hydrator\Strategy\BooleanStrategy;

class Language extends ClassMethods
    implements LanguageInterface
{
    public function __construct($underscoreSeparatedKeys = true)
    {
        parent::__construct($underscoreSeparatedKeys);
        $this->addStrategy('is_enable', new BooleanStrategy('1', '0'));
    }


    public function hydrate(array $data, $object)
    {
        $localeData = $this->getLocaleDataForHydration($data);

        $hydrateData = [
            'id' => $data['id'] ?? '',
            'value' => $data['language'] ?? '',
            'locale' => parent::hydrate($localeData, new LocaleEntity()),
            'is_enable' => $data['is_enable'],
            'description' => $data['description']
        ];

        parent::hydrate($hydrateData, $object);
        (new LanguageDisplayValues())->hydrate($data, $object);

        return $object;
    }


    public function hydrateLanguage(array $data)
    {
        return $this->hydrate($data, new LanguageEntity());
    }


    public function extract($object)
    {
        $extracted = parent::extract($object);

        $result = [
            'id' => $extracted['id'],
            'language' => $extracted['value'],
            'isEnable' => $extracted['is_enable'],
            'description' => $extracted['description'],
            'displayValues' => (new LanguageDisplayValues())->extract(
                $extracted['displayValues'] ?? $extracted['display_values']
            )
        ];

        $extractedLocale = [];
        if ($extracted['locale']) {
            $extractedLocale = $this->getExtractedLocaleData($extracted['locale']);
        }

        return array_merge($result, $extractedLocale);
    }


    protected function getExtractedLocaleData(LocaleEntityInterface $locale)
    {
        return [
            'locale' => [
                'id' => $locale->getId(),
                'value' => $locale->getValue()
            ]
        ];
    }


    protected function getLocaleDataForHydration(array $data)
    {
        $id = $value = null;

        if (array_key_exists('locale', $data)
            && is_array($data['locale'])
        ){
            $id = $data['locale']['id'] ?? '';
            $value = $data['locale']['value'];
        }

        $id = $data['locale_id'] ?? '';
        $value = $data['locale_value'] ?? $data['locale'] ?? '';

        return ['id' => $id, 'value' => $value];
    }

}
