<?php
namespace Languages\Domain\Hydrator;

use Languages\Domain\Entity\Locale as LocaleEntity;
use Languages\Domain\Entity\LocaleInterface as LocaleEntityInterface;
use Languages\Domain\Entity\LanguageInterface as LanguageEntityInterface;
use Languages\Domain\Entity\LanguageDisplayValue as LanguageDisplayValueEntity;
use Languages\Domain\Entity\LanguageDisplayValueInterface as LanguageDisplayValueEntityInterface;
use Languages\Domain\Entity\Collection\DisplayValues as DisplayValuesCollection;
use Zend\Hydrator\ClassMethods;

class LanguageDisplayValues extends ClassMethods
{
    public function hydrate(array $data, $object)
    {
        if (isset($data['displayValue']) || isset($data['display_value'])) {
            $object->getDisplayValues()->add(
                $this->getHydratedDisplayValueObject($data)
            );
        }

        if (isset($data['displayValues']) || isset($data['display_values'])) {
            $object = $this->hydrateDisplayValues(
                ($data['displayValues'] ?? $data['display_values']),
                $object
            );
        }

        return $object;
    }

    public function extract($object)
    {
        return $this->getExtractedDisplayValuesData($object);
    }


    protected function hydrateDisplayValues(
        array $displayValues,
        LanguageEntityInterface $object
    ){
        foreach ($displayValues as $displayValue) {
            $object->getDisplayValues()->add(
                $this->getHydratedDisplayValueObject($displayValue)
            );
        }

        return $object;
    }


    protected function getHydratedDisplayValueObject(array $data)
    {
        $localeData = $this->getLocaleDataForHydration($data);

        $hydrateData = [
            'id'     => $data['displayValueId'] ?? $data['display_value_id'] ?? $data['id'] ?? '',
            'value'  => $data['displayValue'] ?? $data['display_value'] ?? $data['value'],
            'locale' => parent::hydrate($localeData, new LocaleEntity())
        ];

        return parent::hydrate($hydrateData, new LanguageDisplayValueEntity());
    }


    protected function getLocaleDataForHydration(array $data)
    {
        $id = $value = null;

        if (isset($data['locale'])) {
            if (is_array($data['locale'])){
                $id = $data['locale']['id'] ?? '';
                $value = $data['locale']['value'];
            }
            else {
                $id = $data['locale_id'] ?? $data['localeId'];
                $value = $data['locale'];
            }
        }
        else {
            $id = $data['display_value_locale_id'] ?? $data['displayValueLocaleId'];
            $value = $data['display_value_locale'] ?? $data['displayValueLocale'];
        }

        return ['id' => $id, 'value' => $value];
    }


    protected function getExtractedLocaleData(LocaleEntityInterface $locale)
    {
        return [
            'locale' => [
                'id' => $locale->getId(),
                'value' => $locale->getValue()
            ]
        ];
    }


    protected function getExtractedDisplayValueData(
        LanguageDisplayValueEntityInterface $displayValue
    ){
        $extractedLocale = $this->getExtractedLocaleData($displayValue->getLocale());

        return array_merge([
                'id' => $displayValue->getId(),
                'value' => $displayValue->getValue()
            ],
            $extractedLocale
        );
    }


    protected function getExtractedDisplayValuesData(
        DisplayValuesCollection $displayValues
    ){
        $result = [];

        if ($displayValues->count()) {
            $iterator = $displayValues->getIterator();

            foreach($iterator as $displayValue) {
                $result[] = $this->getExtractedDisplayValueData($displayValue);
            }
        }

        return $result;
    }
}
