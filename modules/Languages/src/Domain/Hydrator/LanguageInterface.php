<?php
namespace Languages\Domain\Hydrator;

use Zend\Hydrator\HydratorInterface;

interface LanguageInterface extends HydratorInterface
{
    public function hydrateLanguage(array $data);
}