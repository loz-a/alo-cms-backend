<?php
namespace Languages\Domain\Hydrator;

use Zend\Hydrator\HydrationInterface;

interface SupportedLocaleInterface extends HydrationInterface
{
    public function hydrateLocale(array $data);
}