<?php
namespace Languages\Domain\InputFilter;

use Languages\Domain\Repository\SupportedLocalesInterface as SupportedLocalesRepositoryInterface;
use Zend\InputFilter\{Input, InputFilter};
use Zend\Filter\{StringTrim, StripTags, StripNewlines};

class DisplayValue extends InputFilter
{

    /**
    * @var SupportedLocalesRepositoryInterface
    */
    protected $supportedLocalesRepository;

    public function __construct(
        SupportedLocalesRepositoryInterface $supportedLocalesRepository
    ){
        $this->supportedLocalesRepository = $supportedLocalesRepository;
        $this->init();
    }


    public function init()
    {
      $this
        ->add($this->idInput())
        ->add(
            new Locale($this->supportedLocalesRepository),
            'locale'
        )
        ->add($this->valueInput());
    }


    protected function idInput()
    {
        $id = new Input('id');
        $id->setRequired(true);

        $id->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new StripTags())
            ->attach(new StripNewlines());

        return $id;
    }


    protected function valueInput()
    {
      $value = new Input('value');
      $value->setRequired(true);

      return $value;

    }
}
