<?php
namespace Languages\Domain\InputFilter;

use Interop\Container\ContainerInterface;
use Languages\Options\ModuleOptions;
use Languages\Domain\Repository\LanguagesInterface as LanguagesRepositoryInterface;
use Languages\Domain\Repository\SupportedLocalesInterface as SupportedLocalesRepositoryInterface;

class CreateLanguageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $moduleOptions = $container->get(ModuleOptions::class);
        $languagesRepository = $container->get(LanguagesRepositoryInterface::class);
        $supportLocalesRepository = $container->get(SupportedLocalesRepositoryInterface::class);

        return new CreateLanguage($moduleOptions, $languagesRepository, $supportLocalesRepository);
    }
}