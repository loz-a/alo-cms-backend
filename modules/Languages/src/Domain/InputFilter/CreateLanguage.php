<?php
namespace Languages\Domain\InputFilter;

use Languages\Options\ValidatorInterface as ValidatorOptionsInterface;
use Languages\Domain\Repository\{
    LanguagesInterface as LanguagesRepositoryInterface,
    SupportedLocalesInterface as SupportedLocalesRepositoryInterface
};
use Languages\Domain\Validator\{IsLocaleSupported, NoRecordExists};
use Zend\InputFilter\Input;
use Zend\Filter\{StripNewlines, StringTrim, StripTags};
use Zend\Validator\StringLength;

class CreateLanguage extends AbstractLanguage
{
    /**
     * @var LanguagesRepositoryInterface
     */
    protected $languagesRepository;

    /**
     * @var SupportedLocalesRepositoryInterface
     */
    protected $supportedLocalesRepository;

    public function __construct(
        ValidatorOptionsInterface $options,
        LanguagesRepositoryInterface $languagesRepository,
        SupportedLocalesRepositoryInterface $supportedLocalesRepository
    ){
        parent::__construct($options);

        $this->languagesRepository = $languagesRepository;
        $this->supportedLocalesRepository = $supportedLocalesRepository;

        $this->init();
    }

    public function init()
    {
        $this
            ->add($this->languageInput())
            ->add($this->localeInput())
            ->add($this->descriptionInput())
            ->add($this->isEnableInput());
    }


    protected function languageInput()
    {
        $language = new Input('language');
        $language->setRequired(true);

        $language
            ->getValidatorChain()
            ->attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this->options->getLanguageMaxLength()
            ]));

        $language
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new StripTags())
            ->attach(new StripNewlines());

        return $language;
    }


    protected function localeInput()
    {
        $locale = new Input('locale');
        $locale->setRequired(true);

        $locale
            ->getValidatorChain()
            ->attach(new NoRecordExists([
                'repository' => $this->languagesRepository,
                'key' => 'locale'
            ]))
            ->attach(new IsLocaleSupported($this->supportedLocalesRepository));

        return $locale;
    }
}
