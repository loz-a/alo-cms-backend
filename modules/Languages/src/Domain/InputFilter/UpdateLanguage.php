<?php
namespace Languages\Domain\InputFilter;

use Languages\Options\ValidatorInterface as ValidatorOptionsInterface;
use Languages\Domain\Repository\SupportedLocalesInterface as SupportedLocalesRepositoryInterface;
use Languages\Domain\InputFilter\DisplayValuesCollection as DisplayValuesCollectionInputFilter;
use Zend\InputFilter\Input;
use Zend\Validator\Digits;

class UpdateLanguage extends AbstractLanguage
{
    /**
    * @var SupportedLocalesRepositoryInterface
    */
    protected $supportedLocalesRepository;

    public function __construct(
        ValidatorOptionsInterface $options,
        SupportedLocalesRepositoryInterface $supportedLocalesRepository
    ){
        parent::__construct($options);
        $this->supportedLocalesRepository = $supportedLocalesRepository;

        $this->init();
    }

    public function init()
    {
        $this
            ->add($this->idInput())
            ->add($this->descriptionInput())
            ->add($this->isEnableInput())
            ->add($this->displayValuesInput(), 'displayValues');
    }


    protected function idInput()
    {
        $id = new Input('id');
        $id->setRequired(true);
        $id->getValidatorChain()->attach(new Digits());
        return $id;
    }


    protected function displayValuesInput()
    {        
        $displayValues = new DisplayValuesCollectionInputFilter();
        $displayValues->setInputFilter(new DisplayValue($this->supportedLocalesRepository));
        return $displayValues;
    }
}
