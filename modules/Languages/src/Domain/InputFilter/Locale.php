<?php
namespace Languages\Domain\InputFilter;

use Languages\Domain\Repository\SupportedLocalesInterface as SupportedLocalesRepositoryInterface;
use Languages\Domain\Validator\IsLocaleSupported;
use Zend\InputFilter\{Input, InputFilter};
use Zend\Validator\Digits;

class Locale extends InputFilter
{
    /**
    * @var SupportedLocalesRepositoryInterface
    */
    protected $supportedLocalesRepository;

    /**
     * @param SupportedLocalesRepositoryInterface $supportedLocalesRepository
     */
    public function __construct(
        SupportedLocalesRepositoryInterface $supportedLocalesRepository
    ){
        $this->supportedLocalesRepository = $supportedLocalesRepository;
        $this->init();
    }


    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->add($this->idInput());
        $this->add($this->valueInput());
    }


    protected function idInput()
    {
        $id = new Input('id');
        $id->setRequired(true);
        $id->getValidatorChain()->attach(new Digits());
        return $id;
    }


    protected function valueInput()
    {
        $value = new Input('value');
        $value->setRequired(true);

        $value->getValidatorChain()->attach(
            new IsLocaleSupported($this->supportedLocalesRepository)
        );

        return $value;
    }
}
