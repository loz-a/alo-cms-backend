<?php
namespace Languages\Domain\InputFilter;

use Interop\Container\ContainerInterface;
use Languages\Options\ModuleOptions;
use Languages\Domain\Repository\SupportedLocalesInterface as SupportedLocalesRepositoryInterface;

class UpdateLanguageFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $moduleOptions = $container->get(ModuleOptions::class);
        $supportLocalesRepository = $container->get(SupportedLocalesRepositoryInterface::class);

        return new UpdateLanguage($moduleOptions, $supportLocalesRepository);
    }
}
