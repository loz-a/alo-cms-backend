<?php
namespace Languages\Domain\InputFilter;

use Zend\InputFilter\CollectionInputFilter;

class DisplayValuesCollection extends CollectionInputFilter
{
    public function isValid()
    {
        $valid = parent::isValid();
        $uniques = [];

        foreach ($this->data as $id => $dv) {
            if (!count($uniques)) {
                $uniques[$id] = $dv;
                continue;
            }

            foreach ($uniques as $uId => $uDv) {
                if ($dv['locale']['value'] === $uDv['locale']['value']) {
                    $valid = false;
                    $this->collectionMessages[$id] = sprintf('Display value with locale %s already exists', $dv['locale']['value']);
                }
            }

            if (true === $valid && !array_key_exists($id, $uniques)) {
                $uniques[$id] = $dv;
            }
        }

        return $valid;
    }

}
