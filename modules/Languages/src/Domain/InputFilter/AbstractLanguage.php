<?php
namespace Languages\Domain\InputFilter;

use Components\Validator\CheckboxValue;
use Languages\Options\ValidatorInterface as ValidatorOptionsInterface;
use Zend\InputFilter\{Input, InputFilter};
use Zend\Filter\{StringTrim, StripTags, StripNewlines};
use Zend\Validator\StringLength;

abstract class AbstractLanguage extends InputFilter
{
    /**
     * @var ValidatorOptionsInterface
     */
    protected $options;

    public function __construct(
        ValidatorOptionsInterface $options
    ){
        $this->options = $options;
    }


    protected function descriptionInput()
    {
        $description = new Input('description');
        $description->setRequired(true);

        $description
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new StripTags())
            ->attach(new StripNewlines());

        $description
            ->getValidatorChain()
            ->attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this->options->getLanguageDescriptionMaxLength()
            ]));

        return $description;
    }


    protected function isEnableInput()
    {
        $isEnable = new Input('is_enable');
        $isEnable->setRequired(true);

        $isEnable
            ->getValidatorChain()
            ->attach(new CheckboxValue());

        return $isEnable;
    }

}
