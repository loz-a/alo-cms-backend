<?php
namespace Languages\Domain\Entity;


interface LanguageInterface
{
    public function setId($id);

    public function getId();

    public function setValue($value);

    public function getValue();


    public function setLocale(LocaleInterface $locale);

    /**
     * @return LocaleInterface
     */
    public function getLocale();


    public function setIsEnable(bool $flag);

    public function isEnable();


    public function setDescription($description);

    public function getDescription();


    /**
     * @return Collection\DisplayValues
     */
    public function getDisplayValues();

}