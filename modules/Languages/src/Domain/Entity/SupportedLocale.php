<?php
namespace Languages\Domain\Entity;

class SupportedLocale implements SupportLocaleInterface
{
    protected $name;

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }


    protected $description;

    public function setDescription($description)
    {
        $this->description =  $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

}