<?php
namespace Languages\Domain\Entity\Collection;

use Common\Entity\Collection;
use Languages\Domain\Entity\LanguageDisplayValueInterface;

class DisplayValues extends Collection
{
    public function offsetSet($offset, $value)
    {
        $this->add($value);
    }


    public function add(LanguageDisplayValueInterface $displayValue)
    {
        $this->elements[] = $displayValue;
        return $this;
    }

}