<?php
namespace Languages\Domain\Entity;

class Locale implements LocaleInterface
{
    protected $id;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }


    protected $locale;

    public function setValue($value)
    {
        $this->locale = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->locale;
    }

}