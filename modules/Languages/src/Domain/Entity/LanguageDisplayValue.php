<?php
namespace Languages\Domain\Entity;


class LanguageDisplayValue implements LanguageDisplayValueInterface
{
    protected $id;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }


    protected $locale;

    public function setLocale(LocaleInterface $locale)
    {
        $this->locale = $locale;
        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }


    protected $displayValue;

    public function setValue($value)
    {
        $this->displayValue = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->displayValue;
    }

}