<?php
namespace Languages\Domain\Entity;

interface SupportLocaleInterface
{
    public function setName($name);

    public function getName();

    public function setDescription($description);

    public function getDescription();
}