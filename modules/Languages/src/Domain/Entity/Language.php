<?php
namespace Languages\Domain\Entity;

class Language implements LanguageInterface
{
    protected $id;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }


    protected $language;

    public function setValue($value)
    {
        $this->language = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->language;
    }


    protected $locale;

    public function setLocale(LocaleInterface $locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return LocaleInterface
     */
    public function getLocale()
    {
        return $this->locale;
    }


    protected $isEnable;

    public function setIsEnable(bool $flag)
    {
        $this->isEnable = $flag;
        return $this;
    }

    public function isEnable()
    {
        return $this->isEnable;
    }


    protected $description;

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @var Collection\DisplayValues
     */
    protected $displayValues;

    /**
     * @return Collection\DisplayValues
     */
    public function getDisplayValues()
    {
        if (!$this->displayValues) {
            $this->displayValues = new Collection\DisplayValues();
        }

        return $this->displayValues;
    }
}