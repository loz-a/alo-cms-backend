<?php
namespace Languages\Domain\Entity;


interface LanguageDisplayValueInterface
{
    public function setId($id);

    public function getId();


    public function setLocale(LocaleInterface $localeId);

    /**
     * @return LocaleInterface
     */
    public function getLocale();


    public function setValue($value);

    public function getValue();
}