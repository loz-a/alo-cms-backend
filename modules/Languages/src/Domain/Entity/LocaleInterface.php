<?php
namespace Languages\Domain\Entity;


interface LocaleInterface
{
    public function setId($id);

    public function getId();


    public function setValue($value);

    public function getValue();
}