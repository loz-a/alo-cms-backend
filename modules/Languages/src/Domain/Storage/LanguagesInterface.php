<?php
namespace Languages\Domain\Storage;

interface LanguagesInterface
{
    public function fetchAll();

//    public function findByLocale($locale);

    public function findById($id);

//    public function findOneByIdAndTranslationLocale($id, $locale);

    public function remove(array $language);

    public function create(array $language);

    public function update(array $language);

    public function enableById($id);

    public function disableById($id);

    public function lookupByLanguage($language);

    public function lookupByLocale($locale);
}