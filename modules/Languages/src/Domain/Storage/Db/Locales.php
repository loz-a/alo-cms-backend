<?php
namespace Languages\Domain\Storage\Db;

use App\Domain\Storage\Db\AbstractDbStorage;
use Languages\Domain\Storage\LocalesInterface;

class Locales extends AbstractDbStorage
    implements LocalesInterface
{
    public function lookupByLocale($locale)
    {
        $sql = $this->sql();
        $select = $sql
            ->select($this->table)
            ->columns(['id'])
            ->where(['locale' => $locale])
            ->limit(1);

        $result = $sql->prepareStatementForSqlObject($select)->execute();

        return $result->count() ? $result->current()['id'] : null;
    }


    /**
     * @param string $locale
     * @return int|null
     */
    public function insert($locale)
    {
        $sql = $this->sql();
        $insert = $sql->insert($this->table);
        $insert->values(['locale' => $locale]);
        $result = $sql->prepareStatementForSqlObject($insert)->execute();

        return $result->getAffectedRows() ? $result->getGeneratedValue() : null;
    }


    /**
     * @param int $id
     * @return bool
     */
    public function removeById($id)
    {
        $sql = $this->sql();
        $delete = $sql->delete($this->table)->where(['id' => (int) $id]);
        $result = $sql->prepareStatementForSqlObject($delete)->execute();

        return $result->getGeneratedValue() ? true : false;
    }


    /**
     * @param string $locale
     * @return bool
     */
    public function removeByLocale($locale)
    {
        $sql = $this->sql();
        $delete = $sql->delete($this->table)->where(['locale' => $locale]);
        $result = $sql->prepareStatementForSqlObject($delete)->execute();

        return $result->getGeneratedValue() ? true : false;
    }

}