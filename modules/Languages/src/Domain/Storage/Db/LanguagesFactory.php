<?php
namespace Languages\Domain\Storage\Db;

use Interop\Container\ContainerInterface;
use Languages\Options\ModuleOptions;
use Zend\Db\Adapter\AdapterInterface as DbAdapterInterface;
use Languages\Domain\Storage\LocalesInterface;

class LanguagesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $moduleOptions = $container->get(ModuleOptions::class);

        $tableName = $moduleOptions->getLanguagesTableName();
        $translationTableName = $moduleOptions->getLanguagesTranslationsTableName();

        $localeStorage = $container->get(LocalesInterface::class);
        $dbAdapter = $container->get(DbAdapterInterface::class);

        return new Languages($tableName, $translationTableName, $localeStorage, $dbAdapter);
    }
}