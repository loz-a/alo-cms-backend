<?php
namespace Languages\Domain\Storage\Db\ResultSet;

use Languages\Domain\Hydrator\SupportedLocale as SupportedLocaleHydrator;
use Languages\Domain\Entity\SupportedLocale as SupportedLocaleEntity;
use Zend\Hydrator\HydratorInterface;

class SupportedLocalesHydratingResultSet extends AbstractHydratingResultSet
{
    public function __construct(HydratorInterface $hydrator = null, $objectPrototype = null)
    {
        parent::__construct(
            $hydrator ?: new SupportedLocaleHydrator(),
            $objectPrototype ?: new SupportedLocaleEntity()
        );
    }

    protected function normalizeDataSource(array $dataSource)
    {
        $result = [];

        foreach ($dataSource as $name => $description) {
            $result[] = [
                'name' => $name,
                'description' => $description
            ];
        }

        return $result;
    }

}