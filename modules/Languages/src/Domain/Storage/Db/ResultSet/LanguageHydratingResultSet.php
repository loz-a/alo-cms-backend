<?php
namespace Languages\Domain\Storage\Db\ResultSet;

use Languages\Domain\Hydrator\Language as LanguageHydrator;
use Languages\Domain\Entity\Language as LanguageEntity;
use Zend\Hydrator\HydratorInterface;

class LanguageHydratingResultSet extends AbstractHydratingResultSet
{
    public function __construct(HydratorInterface $hydrator = null, $objectPrototype = null)
    {
        parent::__construct(
            $hydrator ?: new LanguageHydrator(),
            $objectPrototype ?: new LanguageEntity()
        );
    }


    protected function normalizeDataSource(array $dataSource)
    {
        $result = [];

        foreach ($dataSource as $item) {
            $itemId = $item['id'];

            if (array_key_exists($itemId, $result)) {

                if (isset($item['display_value'])) {
                    $result[$itemId]['display_values'][] = [
                        'id' => $item['display_value_id'],
                        'value' => $item['display_value'],
                        'locale_id' => $item['display_value_locale_id'],
                        'locale' => $item['display_value_locale']
                    ];
                }
            }
            else {
                $resultItem = [
                    'id' => $item['id'],
                    'language' => $item['language'],
                    'locale_id' => $item['locale_id'],
                    'locale_value' => $item['locale_value'],
                    'is_enable' => $item['is_enable'],
                    'description' => $item['description'],
                    'display_values' => []
                ];

                if (isset($item['display_value'])) {
                    $resultItem['display_values'][] = [
                        'id' => $item['display_value_id'],
                        'value' => $item['display_value'],
                        'locale_id' => $item['display_value_locale_id'],
                        'locale' => $item['display_value_locale']
                    ];
                }

                $result[$itemId] = $resultItem;
            }

        }

        return array_values($result);
    }
}