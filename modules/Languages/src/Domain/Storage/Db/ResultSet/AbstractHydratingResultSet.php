<?php
namespace Languages\Domain\Storage\Db\ResultSet;

use Zend\Db\ResultSet\HydratingResultSet as ZendHydratingResultSet;
use Zend\Db\Adapter\Driver\ResultInterface;
use ArrayIterator;
use IteratorAggregate;
use Iterator;
use Zend\Db\ResultSet\Exception;

abstract class AbstractHydratingResultSet extends ZendHydratingResultSet
{
    public function initialize($dataSource)
    {
        // reset buffering
        if (is_array($this->buffer)) {
            $this->buffer = [];
        }


        if ($dataSource instanceof ResultInterface) {
            $dataSource = iterator_to_array($dataSource);
        }


        if ($dataSource instanceof ResultInterface
            || is_array($dataSource)
        ){
            $dataSource = $this->normalizeDataSource($dataSource);
            $this->initializeArray($dataSource);

//        if (is_array($dataSource)) {
//            $this->initializeArray($dataSource);
//        } elseif ($dataSource instanceof IteratorAggregate) {
//            $this->dataSource = $dataSource->getIterator();
//        } elseif ($dataSource instanceof Iterator) {
//            $this->dataSource = $dataSource;
        } else {
            throw new Exception\InvalidArgumentException('DataSource provided is not an array, nor does it implement Iterator or IteratorAggregate');
        }

        return $this;
    }


    abstract protected function normalizeDataSource(array $dataSource);


    protected function initializeArray($dataSource)
    {
        // its safe to get numbers from an array
        $first = current($dataSource);
        reset($dataSource);
        $this->fieldCount = count($first);
        $this->dataSource = new ArrayIterator($dataSource);
        $this->buffer = -1; // array's are a natural buffer
    }

}