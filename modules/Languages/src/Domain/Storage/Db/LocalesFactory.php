<?php
namespace Languages\Domain\Storage\Db;

use Interop\Container\ContainerInterface;
use Languages\Options\ModuleOptions;
use Zend\Db\Adapter\AdapterInterface as DbAdapterInterface;

class LocalesFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $moduleOptions = $container->get(ModuleOptions::class);
        $tableName = $moduleOptions->getLocalesTableName();
        $dbAdapter = $container->get(DbAdapterInterface::class);

        return new Locales($tableName, $dbAdapter);
    }
}