<?php
namespace Languages\Domain\Storage\Db\Exception;


class LanguageNotFoundException extends \Exception
    implements ExceptionInterface
{

}