<?php
namespace Languages\Domain\Storage\Db;

use App\Domain\Storage\Db\AbstractDbStorage;
use Languages\Domain\Storage\LanguagesInterface;
use Zend\Db\Adapter\AdapterInterface as DbAdapterInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Exception as BaseException;

class Languages extends AbstractDbStorage
    implements LanguagesInterface
{
    /**
     * @var string
     */
    protected $translationTable;

    /**
     * @var Locales
     */
    protected $localeStorage;

    public function __construct(
        string $table,
        string $translationTable,
        Locales $localeStorage,
        DbAdapterInterface $dbAdapter
    ){
        parent::__construct($table, $dbAdapter);
        $this->translationTable = $translationTable;
        $this->localeStorage = $localeStorage;
    }


    public function getTranslationTable()
    {
        return $this->translationTable;
    }


    public function fetchAll()
    {
        $sql = $this->sql();
        $select = $this->getLanguageSqlSelect($sql);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute($select);
    }


//    public function findByLocale($locale)
//    {
//        $sql = $this->sql();
//        return $this->findBy($sql, 'lang.locale_id', $this->getLocaleSqlSubSelect($sql, $locale));
//    }


    public function findById($id)
    {
        return $this->findBy($this->sql(), 'lang.id', $id);
    }


//    public function findOneByIdAndTranslationLocale($id, $locale)
//    {
//        $sql = $this->sql();
//        $select = $this->getLanguageSqlSelect($sql);
//
//        $select->where([
//            'lang.id' => $id,
//            'lang_transl.locale_id' => $this->getLocaleSqlSubSelect($sql, $locale)
//        ]);
//
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $result = $statement->execute();
//        return $result->count() ? $result->current() : null;
//    }


    public function remove(array $language)
    {
        $connection = $this->dbAdapter->getDriver()->getConnection();
        $sql = $this->sql();
        $affectedRows = null;

        try {
            $connection->beginTransaction();

            $delete = $sql->delete($this->table);
            $delete->where(['id' => $language['id']]);
            $affectedRows = $sql->prepareStatementForSqlObject($delete)->execute()->getAffectedRows();

            if ($affectedRows) {
                $this->localeStorage->removeById($language['locale']['id']);
            }

            $connection->commit();
        }
        catch (BaseException $ex) {
            $connection->rollback();
            throw new BaseException($ex->getMessage());
        }

        return $affectedRows ? $language['id'] : null;
    }


    public function create(array $language)
    {
        $connection = $this->dbAdapter->getDriver()->getConnection();

        try {
            $connection->beginTransaction();

            // insert locale
            $localeId = $this->localeStorage->insert($language['locale']['value']);

            // insert language
            $sql = $this->sql();
            $insert = $sql
                ->insert($this->table)
                ->values([
                    'locale_id' => $localeId,
                    'language' => $language['language'],
                    'is_enable' => $language['is_enable'] ?? $language['isEnable'],
                    'description' => $language['description']
                ]);

            $result = $sql->prepareStatementForSqlObject($insert)->execute();
            $languageId = $result->getGeneratedValue();

            // insert language translate
            if (isset($language['display_values'])) {
                $displayValues = $language['display_values'];

                $insertStr = sprintf('insert into %s (lang_id, locale_id, display_value) values (?, ?, ?)', $this->translationTable);
                $statement = $this->dbAdapter->getDriver()->createStatement($insertStr);

                foreach ($displayValues as $displayValue) {
                    $statement->execute([$languageId, $displayValue['locale']['id'], $displayValue['value']]);
                }
            }

            $connection->commit();
        }
        catch (BaseException $ex) {
            $connection->rollback();
            throw new BaseException($ex->getMessage());
        }

        return $languageId;
    }


    public function update(array $language)
    {
        $connection = $this->dbAdapter->getDriver()->getConnection();
        $languageId = $language['id'];

        try {
            $connection->beginTransaction();

            // get current language with all translations store in db
            $oldLanguageSet = $this->findById($languageId);

            if (!$oldLanguageSet->count()) {
                throw new Exception\LanguageNotFoundException('Language not found');
            }

            // update language
            $sql = $this->sql();
            $update = $sql
                ->update($this->table)
                ->set([
                    'is_enable' => $language['is_enable'] ?? $language['isEnable'],
                    'description' => $language['description']
                ])
                ->where(['id' => $languageId]);

            $sql->prepareStatementForSqlObject($update)->execute();

            $displayValues = $language['displayValues'] ?? [];

            $currentDisplayValuesIds = array_column(iterator_to_array($oldLanguageSet), 'display_value_id');
            $newDisplayValuesIds = array_column($displayValues, 'id');

            $dbDriver = $this->dbAdapter->getDriver();

            $this->insertNewDisplayValues(array_diff($newDisplayValuesIds, $currentDisplayValuesIds), $displayValues, $dbDriver, $languageId);
            $this->updateCurrentDisplayValues(array_intersect($currentDisplayValuesIds, $newDisplayValuesIds), $displayValues, $dbDriver, $languageId);
            $this->deleteOldDisplayValues(array_diff($currentDisplayValuesIds, $newDisplayValuesIds), $dbDriver);

            $connection->commit();
        }
        catch(BaseException $ex) {
            $connection->rollback();
            throw new BaseException($ex->getMessage());
        }

        return $languageId;
    }


    public function lookupByLanguage($language)
    {
        $sql = $this->sql();
        $select = $sql->select();

        $select
            ->columns(['id'])
            ->from($this->table)
            ->where(['language' => $language])
            ->limit(1);

        $result = $sql->prepareStatementForSqlObject($select)->execute();

        return $result->count() ? $result->current()['id'] : null;
    }


    public function lookupByLocale($locale)
    {
        return $this->localeStorage->lookupByLocale($locale);
    }


    public function enableById($id)
    {
        return $this->toggleEnableById($id, true);
    }


    public function disableById($id)
    {
        return $this->toggleEnableById($id, false);
    }


    protected function toggleEnableById($id, $flag)
    {
        $language = $this->findById($id);
        if (!$language->count()) {
            throw new Exception\LanguageNotFoundException('Language not found');
        }

        $language = iterator_to_array($language);

        // if update db not necessary
        if ((true === $flag && $language[0]['is_enable'] === '1')
            or (false === $flag && $language[0]['is_enable'] === '0')
        ){
            return $language;
        }

        $isEnable = $flag ? '1' : '0';

        $sql = $this->sql();
        $update = $sql
            ->update($this->table)
            ->set(['is_enable' => $isEnable])
            ->where(['id' => $id]);

        $result = $sql->prepareStatementForSqlObject($update)->execute();

        if (!$result->getAffectedRows()) {
            throw new Exception\LanguageNotUpdatedException('Language not updated');
        }

        $language = array_map(function($language) use ($isEnable) {
            $language['is_enable'] = $isEnable;
            return $language;
        }, $language);

        return $language;
    }


    protected function getLanguageSqlSelect(Sql $sql)
    {
        $select = $sql->select();

        $select
            ->columns(['id', 'language', 'is_enable', 'description'])
            ->from(['lang' => $this->table])
            ->join(
                ['loc' => $this->localeStorage->getTableName()],
                'lang.locale_id = loc.id',
                ['locale_id' => 'id', 'locale_value' => 'locale']
            )
            ->join(
                ['transl' => $this->translationTable],
                'transl.lang_id = lang.id',
                ['display_value_id' => 'id', 'display_value'],
                'left'
            )
            ->join(
                ['dvloc' => $this->localeStorage->getTableName()],
                new Expression('`transl`.`locale_id` = `dvloc`.`id` AND `transl`.`lang_id` is not null'),
                ['display_value_locale_id' => 'id',	'display_value_locale' => 'locale'],
                'left'
            );

        return $select;
    }


//    protected function getLocaleSqlSubSelect(Sql $sql, $locale)
//    {
//        return $sql
//            ->select($this->localeStorage->getTableName())
//            ->columns(['id'])
//            ->where(['locale' => $locale]);
//    }


    protected function findBy(Sql $sql, $columnName, $value)
    {
        $select = $this->getLanguageSqlSelect($sql);

        $select->where([$columnName => $value]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result->count()) {
            return null;
        }
        return $result;
    }


    protected function insertNewDisplayValues($shouldInsertIds, $displayValues, $dbDriver, $languageId)
    {
        if (count($shouldInsertIds)) {
            $insertStatement = $dbDriver->createStatement(
                sprintf('insert into %s (lang_id, locale_id, display_value) values (?, ?, ?)', $this->translationTable)
            );

            $items = array_filter($displayValues, function($item) use ($shouldInsertIds) {
                return in_array($item['id'], $shouldInsertIds);
            });

            foreach ($items as $item) {
                $insertStatement->execute([$languageId, $item['locale']['id'], $item['value']]);
            }
        }
    }


    protected function updateCurrentDisplayValues($shouldUpdateIds, $displayValues, $dbDriver, $languageId)
    {
        if (count($shouldUpdateIds)) {
            $updateStatement = $dbDriver->createStatement(
                sprintf('update %s set lang_id=?, locale_id=?, display_value=? where id=?', $this->translationTable)
            );

            $items = array_filter($displayValues, function($item) use ($shouldUpdateIds) {
                return in_array($item['id'], $shouldUpdateIds);
            });

            foreach ($items as $item) {
                $updateStatement->execute([$languageId, $item['locale']['id'], $item['value'], $item['id']]);
            }
        }
    }


    protected function deleteOldDisplayValues($shouldDeleteIds, $dbDriver)
    {
        if (count($shouldDeleteIds)) {
            $deleteStatement = $dbDriver->createStatement(
                sprintf(
                    'delete from  %s where id in (%s)',
                    $this->translationTable,
                    implode(',', array_fill(0, count($shouldDeleteIds),'?'))
                )
            );
            $deleteStatement->execute(array_values($shouldDeleteIds));
        }
    }

}
