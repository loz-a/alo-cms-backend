<?php
namespace Languages\Domain\Storage;

interface LocalesInterface
{
    public function lookupByLocale($locale);

    public function insert($locale);

    public function removeById($id);

    public function removeByLocale($locale);
}