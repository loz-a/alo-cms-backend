<?php
use Zend\ServiceManager\Factory\InvokableFactory;
use Languages\Action\Api\ActionFactory;

return [
    'factories' => [
        Languages\Action\Config::class => Languages\Action\ConfigFactory::class,
        Languages\Action\Index::class => Languages\Action\IndexFactory::class,
        Languages\Action\Api\GetLanguages::class => ActionFactory::class,
        Languages\Action\Api\GetLanguage::class => ActionFactory::class,
        Languages\Action\Api\CreateLanguage::class => Languages\Action\Api\SaveLanguageFactory::class,
        Languages\Action\Api\UpdateLanguage::class => Languages\Action\Api\SaveLanguageFactory::class,
        Languages\Action\Api\GetSupportedLocales::class => Languages\Action\Api\GetSupportedLocalesFactory::class,
        Languages\Action\Api\ToggleEnable::class => ActionFactory::class,
        Languages\Action\Api\DeleteLanguage::class => ActionFactory::class,
        Languages\Action\Api\GetInitState::class => Languages\Action\Api\GetInitStateFactory::class,

        Languages\Options\ModuleOptions::class => Languages\Options\Factory::class,

        Languages\Domain\Storage\LanguagesInterface::class => Languages\Domain\Storage\Db\LanguagesFactory::class,
        Languages\Domain\Storage\LocalesInterface::class => Languages\Domain\Storage\Db\LocalesFactory::class,
        Languages\Domain\Storage\Db\ResultSet\LanguageHydratingResultSet::class => InvokableFactory::class,

        Languages\Domain\Storage\Db\ResultSet\SupportedLocalesHydratingResultSet::class => InvokableFactory::class,
        Languages\Domain\Repository\SupportedLocalesInterface::class => Languages\Domain\Repository\SupportedLocalesFactory::class,
        Languages\Domain\Service\SupportedLocalesInterface::class => Languages\Domain\Service\SupportedLocalesFactory::class,

        Languages\Domain\Hydrator\Language::class => InvokableFactory::class,
        Languages\Domain\Repository\LanguagesInterface::class => Languages\Domain\Repository\LanguagesFactory::class,
        Languages\Domain\Service\LanguagesInterface::class => Languages\Domain\Service\LanguagesFactory::class,

        Languages\Domain\InputFilter\CreateLanguage::class => Languages\Domain\InputFilter\CreateLanguageFactory::class,
        Languages\Domain\InputFilter\UpdateLanguage::class => Languages\Domain\InputFilter\UpdateLanguageFactory::class,
    ],
    'aliases' => [
        Languages\Domain\Hydrator\LanguageInterface::class => Languages\Domain\Hydrator\Language::class
    ]
];
