<?php

return [
    'dependencies' => require __DIR__ . '/dependencies.php',

    'routes' => [
        [
            'path' => '/api/languages/get-init-state',
            'middleware' => Languages\Action\Api\GetInitState::class,
            'allowed_methods' => ['GET']
        ],
        [   'name' => 'languages',
            'path' => '/languages',
            'middleware' => Languages\Action\Index::class,
            'allowed_methods' => ['GET']
        ],
        [   'name' => 'languages_config',
            'path' => '/api/languages-config.json',
            'middleware' => Languages\Action\Config::class,
            'allowed_methods' => ['GET']
        ],
        [   'path' => '/api/languages/get',
            'middleware' => Languages\Action\Api\GetLanguages::class,
            'allowed_methods' => ['GET']
        ],
        [   'path' => '/api/languages/get/{id:\d+}',
            'middleware' => Languages\Action\Api\GetLanguage::class,
            'allowed_methods' => ['GET']
        ],
        [   'path' => '/api/languages/save',
            'middleware' => [
                Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
                Languages\Action\Api\UpdateLanguage::class,
                Languages\Action\Api\CreateLanguage::class,
            ],
            'allowed_methods' => ['POST']
        ],[
            'path' => '/api/languages/delete',
            'middleware' => [
                Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
                Languages\Action\Api\DeleteLanguage::class
            ],
            'allowed_methods' => ['POST']
        ],
        [   'path' => '/api/languages/toggle-enable',
            'middleware' => [
                Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware::class,
                Languages\Action\Api\ToggleEnable::class
            ],
            'allowed_methods' => ['POST']
        ],
        [   'path' => '/api/supported-locales/get',
            'middleware' => Languages\Action\Api\GetSupportedLocales::class,
            'allowed_methods' => ['GET']
        ],
    ],

    'templates' => [
        'map' => [
            'languages::index' => __DIR__ . '/../templates/languages/index.phtml'
        ],
        'paths' => [
            'languages' => [__DIR__ . '/../templates/languages']
        ]
    ],

    'admin_navigation' => [
        'languages' => [
            'title' => __('Languages & Locales'),
            'icon'  => 'world',
            'index' => 3
        ]
    ]
];
