<?php
namespace LanguagesTest\Domain\Entity;

use Languages\Domain\Entity\LanguageDisplayValue;
use Languages\Domain\Entity\LanguageDisplayValueInterface;
use Languages\Domain\Entity\Locale;
use Languages\Domain\Entity\LocaleInterface;
use PHPUnit\Framework\TestCase;

class LanguageDisplayValueTest extends TestCase
{
    public function testEntityImplementLanguageDisplayValueInterface()
    {
        $this->assertInstanceOf(LanguageDisplayValueInterface::class, new LanguageDisplayValue());
    }


    public function testPopulateScalarProperty()
    {
        $entity = new LanguageDisplayValue();

        $id = 1;
        $displayValue = 'Ukraine';

        $entity->setId($id);
        $entity->setValue($displayValue);

        $this->assertEquals($id, $entity->getId());
        $this->assertEquals($displayValue, $entity->getValue());
    }


    public function testLanguageLocale()
    {
        $localeId = 1;
        $localeLocale = 'uk_UA';

        $locale = new Locale();
        $locale->setId($localeId);
        $locale->setValue($localeLocale);

        $languageDisplayValue = new LanguageDisplayValue();
        $languageDisplayValue->setLocale($locale);

        $this->assertInstanceOf(LocaleInterface::class, $languageDisplayValue->getLocale());
        $this->assertEquals($localeId, $languageDisplayValue->getLocale()->getId());
        $this->assertEquals($localeLocale, $languageDisplayValue->getLocale()->getValue());
    }

}