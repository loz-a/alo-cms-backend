<?php
namespace LanguagesTest\Domain\Entity;

use Languages\Domain\Entity\Language;
use Languages\Domain\Entity\LanguageDisplayValue;
use Languages\Domain\Entity\LanguageInterface;
use Languages\Domain\Entity\Locale;
use Languages\Domain\Entity\LocaleInterface;
use PHPUnit\Framework\TestCase;

class LanguageTest extends TestCase
{
    public function testEntityImplementLanguageInterface()
    {
        $this->assertInstanceOf(LanguageInterface::class, new Language());
    }

    public function testGetId()
    {
        $entity = new Language();
        $id = 1;
        $entity->setId($id);
        $this->assertEquals($id, $entity->getId());
    }

    public function testPopulateScalarProperty()
    {
        $entity = new Language();

        $id = 1;
        $isEnable = true;
        $language = 'UK';
        $description = 'Ukraine';

        $entity->setId($id);
        $entity->setIsEnable($isEnable);
        $entity->setValue($language);
        $entity->setDescription($description);

        $this->assertEquals($id, $entity->getId());
        $this->assertEquals($isEnable, $entity->isEnable());
        $this->assertEquals($language, $entity->getValue());
        $this->assertEquals($description, $entity->getDescription());
    }


    public function testLanguageLocale()
    {
        $localeId = 1;
        $localeLocale = 'uk_UA';

        $locale = new Locale();
        $locale->setId($localeId);
        $locale->setValue($localeLocale);

        $language = new Language();
        $language->setLocale($locale);

        $this->assertInstanceOf(LocaleInterface::class, $language->getLocale());
        $this->assertEquals($localeId, $language->getLocale()->getId());
        $this->assertEquals($localeLocale, $language->getLocale()->getValue());
    }


    public function testDisplayValuesAsCollection()
    {
        $ukr = (new LanguageDisplayValue())
            ->setId(1)
            ->setLocale((new Locale())->setId(1)->setValue('uk_UA'))
            ->setValue('Ukrane');

        $usa = (new LanguageDisplayValue())
                ->setId(2)
                ->setLocale((new Locale())->setId(2)->setValue('en_US'))
                ->setValue('USA');

        $eng = (new LanguageDisplayValue())
                ->setId(3)
                ->setLocale((new Locale())->setId(3)->setValue('en_GB'))
                ->setValue('Great Britain');

        $language = new Language();

        $collection = $language->getDisplayValues();
        $collection[] = $ukr;
        $collection[] = $usa;
        $collection[] = $eng;

        $this->assertEquals(3, $language->getDisplayValues()->count());

        $this->assertEquals($ukr->getId(), $language->getDisplayValues()[0]->getId());
        $this->assertEquals($ukr->getLocale()->getId(), $language->getDisplayValues()[0]->getLocale()->getId());
        $this->assertEquals($ukr->getLocale()->getValue(), $language->getDisplayValues()[0]->getLocale()->getValue());
        $this->assertEquals($ukr->getValue(), $language->getDisplayValues()[0]->getValue());

        $this->assertEquals($usa->getId(), $language->getDisplayValues()[1]->getId());
        $this->assertEquals($usa->getLocale()->getId(), $language->getDisplayValues()[1]->getLocale()->getId());
        $this->assertEquals($usa->getLocale()->getValue(), $language->getDisplayValues()[1]->getLocale()->getValue());
        $this->assertEquals($usa->getValue(), $language->getDisplayValues()[1]->getValue());

        $this->assertEquals($eng->getId(), $language->getDisplayValues()[2]->getId());
        $this->assertEquals($eng->getLocale()->getId(), $language->getDisplayValues()[2]->getLocale()->getId());
        $this->assertEquals($eng->getLocale()->getValue(), $language->getDisplayValues()[2]->getLocale()->getValue());
        $this->assertEquals($eng->getValue(), $language->getDisplayValues()[2]->getValue());
    }
}