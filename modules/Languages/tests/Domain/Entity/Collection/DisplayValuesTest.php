<?php
namespace LanguagesTest\Domain\Entity\Collection;

use Languages\Domain\Entity\Collection\DisplayValues;
use Languages\Domain\Entity\LanguageDisplayValue;
use Languages\Domain\Entity\Locale;
use PHPUnit\Framework\TestCase;
use TypeError;
use ArrayIterator;

class DisplayValuesTest extends TestCase
{
    protected function getFixtures()
    {
        yield 'ukr' => (new LanguageDisplayValue())
            ->setId(1)
            ->setLocale((new Locale())->setId(1)->setValue('uk_UA'))
            ->setValue('Ukrane');

        yield 'usa' => (new LanguageDisplayValue())
            ->setId(2)
            ->setLocale((new Locale())->setId(2)->setValue('en_US'))
            ->setValue('USA');

        yield 'eng' => (new LanguageDisplayValue())
            ->setId(3)
            ->setLocale((new Locale())->setId(3)->setValue('en_GB'))
            ->setValue('Great Britain');
    }


    protected function populatCollection()
    {
        $collection = new DisplayValues();

        foreach ($this->getFixtures() as $displayValue) {
            $collection[] = $displayValue;
        }

        return $collection;
    }


    public function testOffsetSetAndOffsetGet()
    {
        $fixtures = $this->getFixtures();

        $ukr = $fixtures->current();
        $fixtures->next();
        $usa = $fixtures->current();

        $collection = new DisplayValues();

        $collection->add($ukr);
        $collection[] = $usa;

        $this->assertEquals($ukr, $collection[0]);
        $this->assertEquals($usa, $collection[1]);
    }


    public function testNonExistingOffsetGet()
    {
        $collection = new DisplayValues();
        $this->assertNull($collection[0]);
    }


    public function testOffsetGetByReference()
    {
        $collection = new DisplayValues();
        $collection[] = $this->getFixtures()->current();

        $newId = 55;
        $displayValue = $collection[0];
        $displayValue->setId(55);

        $this->assertEquals($newId, $collection[0]->getId());
        $this->assertSame($displayValue->getId(), $collection[0]->getId());
    }


    public function testInvalidOffsetSet()
    {
        $this->expectException(TypeError::class);
        $collection = new DisplayValues();
        $collection[] = new Locale();
    }


    public function testInvalidAdd()
    {
        $this->expectException(TypeError::class);
        $collection = new DisplayValues();
        $collection->add(new Locale());
    }


    public function testUnset()
    {
        $collection = new DisplayValues();
        $displayValue = $this->getFixtures()->current();

        $collection[] = $displayValue;
        $removedDisplayValue = $collection->offsetUnset(0);

        $this->assertEquals(0, $collection->count());
        $this->assertSame($displayValue, $removedDisplayValue);
    }


    public function testUnsetReturnNull()
    {
        $collection = new DisplayValues();
        $removed = $collection->offsetUnset(0);

        $this->assertNull($removed);
    }


    public function testIsset()
    {
        $collection = new DisplayValues();
        $collection[] = $this->getFixtures()->current();
        $this->assertTrue(isset($collection[0]));
    }


    public function testCount()
    {
        $this->assertEquals(3, $this->populatCollection()->count());
    }


    public function testClear()
    {
        $collection = $this->populatCollection();

        $this->assertEquals(3, $collection->count());
        $collection->clear();
        $this->assertEquals(0, $collection->count());
    }

    public function testGetIteratorInstance()
    {
        $collection = new DisplayValues();
        $this->assertInstanceOf(ArrayIterator::class, $collection->getIterator());
    }


    public function testGetIterator()
    {
        $collection = $this->populatCollection();
        $iterator = $collection->getIterator();
        $iterator2 = new ArrayIterator($collection->toArray());
        $this->assertEquals($iterator, $iterator2);
    }
}