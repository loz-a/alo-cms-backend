<?php
namespace LanguagesTest\Domain\Entity;

use Languages\Domain\Entity\Locale;
use Languages\Domain\Entity\LocaleInterface;
use PHPUnit\Framework\TestCase;

class LocaleTest extends TestCase
{
    public function testEntityImplementLocaleInterface()
    {
        $this->assertInstanceOf(LocaleInterface::class, new Locale());
    }


    public function testPopulateScalarProperty()
    {
        $entity = new Locale();

        $id = 1;
        $locale = 'en_UK';

        $entity->setId($id);
        $entity->setValue($locale);

        $this->assertEquals($id, $entity->getId());
        $this->assertEquals($locale, $entity->getValue());
    }
}