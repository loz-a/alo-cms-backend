<?php
namespace LanguagesTest\Domain\Hydrator;

use Languages\Domain\Entity\Language;
use PHPUnit\Framework\TestCase;
use Languages\Domain\Hydrator\Language as LanguageHydrator;
use TypeError;

class LanguageTest extends TestCase
{
    public function testHydrateNewLanguage()
    {
        $hydrator = new LanguageHydrator();

        $data = [
            'language'                => 'uk',
            'locale_id'               => 1,
            'locale_value'            => 'uk_UA',
            'is_enable'               => '1',
            'description'             => 'Ukraine',
            'display_value'           => 'Ukr',
            'display_value_locale_id' => 2,
            'display_value_locale'    => 'en_US'
        ];

        /**
         * @var Language $language
         */
        $language = $hydrator->hydrate($data, new Language());

        $this->assertEmpty($language->getId());
        $this->assertEquals($data['language'], $language->getValue());
        $this->assertTrue($language->isEnable());
        $this->assertEquals($data['description'], $language->getDescription());

        $this->assertEquals(
            $data['locale_id'], $language->getLocale()->getId()
        );
        $this->assertEquals(
            $data['locale_value'], $language->getLocale()->getValue()
        );

        $this->assertEquals(
            $data['display_value'],
            $language->getDisplayValues()[0]->getValue()
        );
        $this->assertEquals(
            $data['display_value_locale_id'],
            $language->getDisplayValues()[0]->getLocale()->getId()
        );
        $this->assertEquals(
            $data['display_value_locale'],
            $language->getDisplayValues()[0]->getLocale()->getValue()
        );
    }


    public function testHydrateUpdatedLanguage()
    {
        $hydrator = new LanguageHydrator();

        $data = [
            'id' => 1,
            'language'      => 'uk',
            'locale_id'     => 1,
            'locale_value'  => 'uk_UA',
            'is_enable'     => '1',
            'description'   => 'Ukraine',
            'display_values' => [
                [
                    'id' => 1,
                    'value'     => 'Укр',
                    'locale_id' => 1,
                    'locale'    => 'uk_UA'
                ],
                [
                    'id' => 2,
                    'value'     => 'Ukr',
                    'locale_id' => 2,
                    'locale'    => 'en_US'
                ],
            ]
        ];

        $language = $hydrator->hydrate($data, new Language());

        $this->assertEquals($data['id'], $language->getId());

        $this->assertEquals(
            $data['display_values'][0]['id'],
            $language->getDisplayValues()[0]->getId()
        );
        $this->assertEquals(
            $data['display_values'][1]['id'],
            $language->getDisplayValues()[1]->getId()
        );
    }


    public function testHydrateNewLanguageWithMultiDisplayValues()
    {
        $hydrator = new LanguageHydrator();

        $data = [
            'language'      => 'uk',
            'locale_id'     => 1,
            'locale_value'  => 'uk_UA',
            'is_enable'     => '1',
            'description'   => 'Ukraine',
            'display_values' => [
                [
                    'value'     => 'Укр',
                    'locale_id' => 1,
                    'locale'    => 'uk_UA'
                ],
                [
                    'value'     => 'Ukr',
                    'locale_id' => 2,
                    'locale'    => 'en_US'
                ],
            ]
        ];

        /**
         * @var Language $language
         */
        $language = $hydrator->hydrate($data, new Language());

        $this->assertEquals(
            $data['display_values'][0]['value'],
            $language->getDisplayValues()[0]->getValue()
        );
        $this->assertEquals(
            $data['display_values'][0]['locale_id'],
            $language->getDisplayValues()[0]->getLocale()->getId()
        );
        $this->assertEquals(
            $data['display_values'][0]['locale'],
            $language->getDisplayValues()[0]->getLocale()->getValue()
        );


        $this->assertEquals(
            $data['display_values'][1]['value'],
            $language->getDisplayValues()[1]->getValue()
        );
        $this->assertEquals(
            $data['display_values'][1]['locale_id'],
            $language->getDisplayValues()[1]->getLocale()->getId()
        );
        $this->assertEquals(
            $data['display_values'][1]['locale'],
            $language->getDisplayValues()[1]->getLocale()->getValue()
        );
    }


    public function testDisplayValuesItemIsScalarType()
    {
        $this->expectException(TypeError::class);

        $data = [
            'language'      => 'uk',
            'locale_id'     => 1,
            'locale_value'  => 'uk_UA',
            'is_enable'     => '1',
            'description'   => 'Ukraine',
            'display_values' => ['displayValue']
        ];

        $language = (new LanguageHydrator())->hydrate($data, new Language());
    }


    public function testExtractLanguage()
    {
        $hydrator = new LanguageHydrator();

        $data = [
            'language' => 'uk',
            'locale_id' => 1,
            'locale_value' => 'uk_UA',
            'is_enable' => '1',
            'description' => 'Ukraine',
            'display_value' => 'Ukr',
            'display_value_locale_id' => 2,
            'display_value_locale' => 'en_US'
        ];

        $language = $hydrator->hydrate($data, new Language());

        $extractedData = $hydrator->extract($language);

        $this->assertEmpty($extractedData['id']);
        $this->assertEquals($extractedData['language'], $data['language']);
        $this->assertEquals($extractedData['is_enable'], $data['is_enable']);
        $this->assertEquals($data['description'], $data['description']);

        $this->assertEquals($extractedData['locale']['id'], $data['locale_id']);
        $this->assertEquals($extractedData['locale']['value'], $data['locale_value']);

        $this->assertEquals($extractedData['display_values'][0]['value'], $data['display_value']);
        $this->assertEquals($extractedData['display_values'][0]['locale']['id'], $data['display_value_locale_id']);
        $this->assertEquals($extractedData['display_values'][0]['locale']['value'], $data['display_value_locale']);
    }
}