<?php
namespace LanguagesTest\Domain\Storage\Db;

use PHPUnit\Framework\TestCase;
use Zend\Db\Adapter\Adapter;
use Languages\Domain\Storage\Db\Languages as LanguagesStorage;
use Languages\Domain\Storage\Db\Locales as LocalesStorage;

class DbTestCase extends TestCase
{
    /**
     * @var LanguagesStorage
     */
    protected static $LanguagesStorage;

    /**
     * @var LocalesStorage
     */
    protected static $localesStorage;

    /**
     * @var Adapter
     */
    protected static $dbAdapter;

    public static function setUpBeforeClass()
    {
        $dbAdapter = new Adapter(
            [
                'driver' => 'pdo',
                'dsn'    => 'mysql:dbname=e-shop-test;host=localhost;charset=utf8',
                'user'   => 'root',
                'pass'   => 'AM23naRmysql',
            ]
        );

        $localesStorage = new LocalesStorage('locales', $dbAdapter);

        $languagesStorage = new LanguagesStorage(
            'languages', 'languages_translations', $localesStorage, $dbAdapter
        );

        self::$dbAdapter = $dbAdapter;
        self::$LanguagesStorage = $languagesStorage;
        self::$localesStorage = $localesStorage;

        self::$dbAdapter->query(
            'CREATE TABLE IF NOT EXISTS `' . $localesStorage->getTableName() . '` (
              `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
              `locale` varchar(7) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `locale` (`locale`)
            ) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;',
            Adapter::QUERY_MODE_EXECUTE
        );

        self::$dbAdapter->query(
            "CREATE TABLE IF NOT EXISTS `" . $languagesStorage->getTableName() . "` (
              `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
              `locale_id` tinyint(3) unsigned NOT NULL,
              `language` varchar(3) NOT NULL,
              `is_enable` tinyint(1) unsigned NOT NULL DEFAULT '1',
              `description` varchar(255) NOT NULL,
              PRIMARY KEY (`id`,`locale_id`),
              UNIQUE KEY `locale_id_language` (`locale_id`,`language`),
              CONSTRAINT `FK_languages_locales` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;",
            Adapter::QUERY_MODE_EXECUTE
        );

        self::$dbAdapter->query(
            "CREATE TABLE IF NOT EXISTS `"
            . $languagesStorage->getTranslationTable() . "` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `lang_id` tinyint(3) unsigned NOT NULL,
              `locale_id` tinyint(3) unsigned NOT NULL,
              `display_value` varchar(50) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `FK_languages_translations_languages` (`lang_id`),
              KEY `FK_languages_translations_locales` (`locale_id`),
              CONSTRAINT `FK_languages_translations_languages` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
              CONSTRAINT `FK_languages_translations_locales` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;",
            Adapter::QUERY_MODE_EXECUTE
        );
    }


    protected function setUp()
    {
        self::$dbAdapter->getDriver()->getConnection()->beginTransaction();

        self::$dbAdapter->query(
            "INSERT INTO `" . self::$localesStorage->getTableName() . "` (`id`, `locale`) VALUES	(41, 'ar_LY'), (35, 'ar_SA'), (31, 'ast_ES'), (42, 'az_TR'), (26, 'bg_BG'),
	                (13, 'de_DE'), (8, 'en_US'), (19, 'fil_PH'), (18, 'fr_CH'), (33, 'hy_AM'),	(32, 'sq_AL'),	(20, 'ug_CN'), (10, 'uk_UA');",
            Adapter::QUERY_MODE_EXECUTE
        );

        self::$dbAdapter->query(
            "INSERT INTO `" . self::$LanguagesStorage->getTableName() . "` (`id`, `locale_id`, `language`, `is_enable`, `description`) VALUES
                (12, 8, 'en', 1, 'English (United States)'), (15, 10, 'uk', 0, 'Ukrainian (Ukraine)'), (17, 13, 'de', 1, 'GERMAN (Germany)'),
                (20, 18, 'ch', 1, 'French (Switzerland)1'), (21, 19, 'ph', 1, 'Filipino (Philippines)'), (22, 20, 'cn', 1, 'Uighur (China)'),
                (25, 26, 'bg', 1, 'Bulgarian (Bulgaria)'), (28, 31, 'ast', 0, 'Asturian (Spain)'), (29, 32, 'sq', 1, 'Albanian (Albania)'),
                (30, 33, 'hy', 1, 'Armenian (Armenia)'), (31, 35, 'ar', 0, 'Arabic (Saudi Arabia)'), (33, 41, 'ar', 0, 'Arabic (Libya)'),
                (34, 42, 'az', 1, 'Azerbaijani (Turkey)');",
            Adapter::QUERY_MODE_EXECUTE
        );

        self::$dbAdapter->query(
            "INSERT INTO `" . self::$LanguagesStorage->getTranslationTable() . "` (`id`, `lang_id`, `locale_id`, `display_value`) VALUES
                (7, 12, 8, 'EN'), (9, 15, 10, 'УКР'), (13, 17, 10, 'НІМ'), (14, 15, 8, 'UKR'), (17, 20, 10, 'Французька (Швейцарія)'),
                (18, 12, 10, 'Англ.'), (19, 12, 13, 'Englisch');",
            Adapter::QUERY_MODE_EXECUTE
        );
    }


    public function tearDown()
    {
        self::$dbAdapter->getDriver()->getConnection()->rollback();
    }


    public static function tearDownAfterClass()
    {
        self::$dbAdapter = null;
        self::$LanguagesStorage = null;
        self::$localesStorage = null;
    }

}