<?php

namespace LanguagesTest\Domain\Storage\Db\ResultSet;

use Languages\Domain\Storage\Db\ResultSet\LanguageHydratingResultSet;
use LanguagesTest\Domain\Storage\Db\DbTestCase;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Languages\Domain\Hydrator\Language as LanguageHydrator;
use Languages\Domain\Entity\Language;

class HydratingResultSetTest extends DbTestCase
{

    public function testHydratingResultSetCorrectCount()
    {
        $sql = new Sql(self::$dbAdapter);
        $select = $sql->select();

        $select
            ->columns(['languagesCount' => new Expression('COUNT(*)')])
            ->from(self::$LanguagesStorage->getTableName());

        $statement = $sql->prepareStatementForSqlObject($select);
        $languagesCount = $statement->execute($select)->current()['languagesCount'];

        $this->assertEquals($languagesCount, $this->getLanguagesResultSet()->count());
    }


    public function testDisplayValuesCorrectCount()
    {
        $resultSet = $this->getLanguagesResultSet();

        $sql = new Sql(self::$dbAdapter);
        $select = $sql->select();

        $select
            ->columns(['language', 'displayValuesCount' => new Expression('COUNT(*)')])
            ->from(['lang' => self::$LanguagesStorage->getTableName()])
            ->join(
                ['tr' => self::$LanguagesStorage->getTranslationTable()],
                'tr.lang_id = lang.id',
                [],
                'right'
            )
            ->group('tr.lang_id');

        $statement = $sql->prepareStatementForSqlObject($select);
        $displayValuesCountFromDb = iterator_to_array($statement->execute($select));

        $isCorrect = true;
        foreach ($resultSet as $language) {
            $languageName = $language->getValue();
            $displayValuesCount = $language->getDisplayValues()->count();

            foreach ($displayValuesCountFromDb as $countFromDb) {
                if ($languageName === $countFromDb['language']
                    && $displayValuesCount != $countFromDb['displayValuesCount']
                ){
                    $isCorrect = false;
                }
            }

        }

        $this->assertTrue($isCorrect);
    }


    public function testLanguagesResultSetValuesEqualsValuesFromDb()
    {
        $resultSet = $this->getLanguagesResultSet();

        $sql = new Sql(self::$dbAdapter);
        $select = $sql->select();

        $select
            ->columns(['language'])
            ->from(self::$LanguagesStorage->getTableName());

        $statement = $sql->prepareStatementForSqlObject($select);
        $languaesFromDb = iterator_to_array($statement->execute($select));

        $onlyValuesFromDb = [];
        foreach ($languaesFromDb as $lfdb) {
            $onlyValuesFromDb[] = $lfdb['language'];
        }

        $isEquals = true;
        foreach ($resultSet as $language) {
            if (!in_array($language->getValue(), $onlyValuesFromDb)) {
                $isEquals = false;
            }
        }

        $this->assertTrue($isEquals);
    }


    protected function getLanguagesResultSet()
    {
        $sql = new Sql(self::$dbAdapter);
        $select = $sql->select();

        $select
            ->columns(['id', 'language', 'is_enable', 'description'])
            ->from(['lang' => self::$LanguagesStorage->getTableName()])
            ->join(
                ['loc' => self::$localesStorage->getTableName()],
                'lang.locale_id = loc.id',
                ['locale_id' => 'id', 'locale_value' => 'locale']
            )
            ->join(
                ['transl' => self::$LanguagesStorage->getTranslationTable()],
                'transl.lang_id = lang.id',
                ['display_value_id' => 'id', 'display_value'],
                'left'
            )
            ->join(
                ['dvloc' => self::$localesStorage->getTableName()],
                new Expression(
                    '`transl`.`locale_id` = `dvloc`.`id` AND `transl`.`lang_id` is not null'
                ),
                ['display_value_locale_id' => 'id',
                 'display_value_locale'    => 'locale'],
                'left'
            );

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute($select);

        $resultSet = new LanguageHydratingResultSet(
            new LanguageHydrator(), new Language()
        );
        $resultSet->initialize($result);

        return $resultSet;
    }

}