<?php
namespace Auth\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class Login extends InputFilter
{
    public function init()
    {
        $this->add([
            'name'     => 'login',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim']
            ]
        ]);

        $this->add([
            'name'     => 'password',
            'required' => true
        ]);
    } // init()
}