<?php
namespace Auth\Form;

use Auth\Form\InputFilter\Login as LoginInputFilter;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Form\InputFilter\Login as LoginFilter;

class LoginFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $translator = $container->get(TranslatorInterface::class);

        $inputFilter = $container->get(LoginFilter::class);
        $inputFilter->init();

        $form = new Login($translator);
        $form->setInputFilter($inputFilter);
        $form->init();

        return $form;
    } // __invoke()
}