<?php
namespace Auth\Action\Api;

use Auth\Form\InputFilter\Login as LoginFilter;
use Auth\Service\Auth as AuthService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Translator\TranslatorTrait;
use Auth\Responder\Api\Unauthorized as UnauthorizedResponder;

class Login
{
    use TranslatorTrait;

    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var LoginFilter
     */
    private $loginFilter;

    /**
     * @var UnauthorizedResponder
     */
    private $responder;

    public function __construct(
        AuthService $authService,
        LoginFilter $loginFilter,
        TranslatorInterface $translator,
        UnauthorizedResponder $responder
    ){
        $this->authService = $authService;
        $this->loginFilter = $loginFilter;
        $this->translator  = $translator;
        $this->responder   = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $params = $request->getParsedBody();
        $this->loginFilter->setData($params);

        if ($this->loginFilter->isValid()) {
            $result = $this->authService->login($params['login'], $params['password']);

            if ($result->isValid()) {
                return $next($request, $response);
            }
        }

        sleep(1); // small bruteforce shield
        return $this->responder->__invoke([
            'data' => [
                'error' => $this->translate('Authorization error. Please check login or/and password')
            ]
        ]);

    } // __invoke()
}