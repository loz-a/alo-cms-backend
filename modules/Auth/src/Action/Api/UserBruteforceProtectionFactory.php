<?php
namespace Auth\Action\Api;

use Auth\Options\ModuleOptions;
use Auth\Service\BruteforceProtection\User as UserProtectionService;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Responder\Api\Unauthorized as UnauthorizedResponder;

class UserBruteforceProtectionFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $options    = $container->get(ModuleOptions::class);
        $service    = $container->get(UserProtectionService::class);
        $translator = $container->get(TranslatorInterface::class);
        $responder  = new UnauthorizedResponder();

        return new UserBruteforceProtection($options, $service, $translator, $responder);
    }
}