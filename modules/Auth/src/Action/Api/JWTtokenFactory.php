<?php
namespace Auth\Action\Api;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Auth\Responder\Api\Login as LoginResponder;
use Auth\Options\ModuleOptions;

class JWTtokenFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $apiOptions = $container->get(ModuleOptions::class)->getApi();
        $router     = $container->get(RouterInterface::class);
        $responder  = new LoginResponder();

        return new JWTtoken($apiOptions->getJwtOptions(), $router, $responder);
    } // __invoke()
}