<?php
namespace Auth\Action\Api;

use Auth\Form\InputFilter\Login as LoginFilter;
use Auth\Service\Auth as AuthService;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Responder\Api\Unauthorized as UnauthorizedResponder;

class LoginFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $loginFilter = $container->get(LoginFilter::class);
        $translator  = $container->get(TranslatorInterface::class);
        $responder   = new UnauthorizedResponder();

        return new Login($authService, $loginFilter, $translator, $responder);
    } // __invoke()
}