<?php
namespace Auth\Action\Api;

use Auth\Service\Auth as AuthService;
use Interop\Container\ContainerInterface;
use App\Responder\JsonResponder;

class LogoutFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new Logout(
            $container->get(AuthService::class),
            new JsonResponder()
        );
    } // __invoke()
}