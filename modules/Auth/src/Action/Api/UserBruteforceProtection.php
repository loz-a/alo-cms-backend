<?php
namespace Auth\Action\Api;

use Auth\Options\AuthInterface as AuthOptionsInterface;
use Auth\Service\BruteforceProtection\User as UserProtectionService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Translator\TranslatorTrait;
use Auth\Responder\Api\Unauthorized as UnauthorizedResponder;

class UserBruteforceProtection
{
    use TranslatorTrait;

    /**
     * @var AuthOptionsInterface
     */
    protected $authOptions;
    /**
     * @var UserProtectionService
     */
    protected $userProtectionService;

    /**
     * @var UnauthorizedResponder
     */
    private $responder;

    public function __construct(
        AuthOptionsInterface $authOptions,
        UserProtectionService $service,
        TranslatorInterface $translator,
        UnauthorizedResponder $responder
    ){
        $this->authOptions = $authOptions;
        $this->userProtectionService = $service;
        $this->translator  = $translator;
        $this->responder   = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $params = $request->getParsedBody();
        $login  = $params['login'] ?? null;

        if (!$login) {
            return $this->responder->__invoke([
                'data' => [
                    'error' => $this->translate('Authorization error. Please check login or/and password')
                ]
            ]);
        }

        $isLocked = $this->userProtectionService->isLocked($login);
        $waitTime = $this->authOptions->getWaitTime();

        if ($isLocked) {
            return $this->responder->__invoke([
                'data' => [
                    'error' => sprintf($this->translate('Authorization error. Please, expect the next %s seconds to login'), $waitTime)
                ]
            ]);
        }

        $result = $next($request, $response);

        if ($result instanceof ResponseInterface &&
            $login &&
            $result->getStatusCode() === UnauthorizedResponder::UNAUTHORIZED_CODE
        ) {
            $this->userProtectionService->increaseTriesCounter($login);
        }

        return $result;
    }

}