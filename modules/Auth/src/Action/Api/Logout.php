<?php
namespace Auth\Action\Api;

use Auth\Service\Auth as AuthService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Responder\JsonResponder;

class Logout
{
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(
        AuthService $authService,
        JsonResponder $responder
    ){
        $this->authService = $authService;
        $this->responder   = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $this->authService->logout();

        return $this->responder->getResponse([
            'data' => [
                'success' => 'true'
            ]
        ]);
    } // __invoke()
}