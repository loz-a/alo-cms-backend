<?php
namespace Auth\Action\Api;

use Zend\Expressive\Router\RouterInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Auth\Responder\Api\Login as LoginResponder;
use Auth\Options\Api\JWTOptionsInterface;
use Auth\Service\Jwt\JwtTokenBuilder;

class JWTtoken
{
    /**
     * @var JWTOptionsInterface
     */
    private $jwtOptions;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var LoginResponder
     */
    private $responder;

    public function __construct(
        JWTOptionsInterface $jwtOptions,
        RouterInterface $router,
        LoginResponder $responder
    ){
        $this->jwtOptions = $jwtOptions;
        $this->router     = $router;
        $this->responder  = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $tokenData = [
            'login'     => $request->getParsedBody()['login'],
            'userAgent' => $request->getServerParams()['HTTP_USER_AGENT']
        ];

        $token = (JwtTokenBuilder::build($this->jwtOptions, $tokenData));

        return $this->responder->__invoke([
            'data' => [
                'token' => strval($token),
                'redirectRoute' => $this->router->generateUri('admin')
            ]
        ]);
    } // __invoke()

}