<?php
namespace Auth\Action;

use Auth\Options\ModuleOptions as AuthOptionsInterface;
use Auth\Form\Login as LoginForm;
use Auth\Responder\Login as LoginResponder;
use Auth\Service\BruteforceProtection\User as UserProtectionService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\I18n\Translator\TranslatorInterface;

class UserBruteforceProtectionFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $options    = $container->get(AuthOptionsInterface::class);
        $service    = $container->get(UserProtectionService::class);
        $loginForm  = $container->get(LoginForm::class);
        $translator = $container->get(TranslatorInterface::class);
        $template   = $container->get(TemplateRendererInterface::class);
        $responder  = new LoginResponder($template);

        return new UserBruteforceProtection($options, $service, $loginForm, $translator, $responder);
    }
}