<?php
namespace Auth\Action;

use App\Responder\JsonResponder;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;

class ConfigFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $translator = $container->get(TranslatorInterface::class);
        $responder  = new JsonResponder();

        return new Config($translator, $responder);
    } // __invoke()
}