<?php
namespace Auth\Action;

use App\Responder\JsonResponder;
use Auth\Translator\TranslatorTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\I18n\Translator\Translator;

class Config
{
    use TranslatorTrait;

    /**
     * @var JsonResponder
     */
    private $responder;

    public function __construct(Translator $translator, JsonResponder $responder)
    {
        $this->translator = $translator;
        $this->responder  = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ) {
        return $this->responder->getResponse([
            'data' => [
                'translations' => $this->getTranslateMessages()
            ]
        ]);
    } // __invoke()
}