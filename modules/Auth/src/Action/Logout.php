<?php
namespace Auth\Action;

use Auth\Service\Auth as AuthService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class Logout
{
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(
        AuthService $authService,
        RouterInterface $router
    ){
        $this->authService = $authService;
        $this->router      = $router;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $this->authService->logout();

        return new RedirectResponse(
            $this->router->generateUri('home')
        );
    } // __invoke()
}