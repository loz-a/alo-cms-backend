<?php
namespace Auth\Options;


interface DbTableNamesInterface
{
    public function setUsersTableName($tableName);
    public function getUsersTableName();

    public function setLockedTableName($tableName);
    public function getLockedTableName();
}