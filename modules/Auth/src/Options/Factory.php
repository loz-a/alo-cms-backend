<?php
namespace Auth\Options;

use Interop\Container\ContainerInterface;

class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $options = $config['auth_options'] ?? [];

        $moduleOptions = new ModuleOptions($options);
        $moduleOptions->setApi($config['api'] ?? []);

        return $moduleOptions;
    }
}