<?php
namespace Auth\Options;

use Auth\Options\Api\OptionsInterface;

interface ApiOptionsInterface
{
    public function setApi(array $options);
    public function getApi() : OptionsInterface;
}