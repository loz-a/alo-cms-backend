<?php
namespace Auth\Options;


interface PasswordInterface
{
    public function setPasswordMinLength($minLength);
    public function getPasswordMinLength();
}