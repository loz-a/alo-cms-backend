<?php
namespace Auth\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements AuthInterface, PasswordInterface, DbTableNamesInterface, ApiOptionsInterface
{
    protected $usersTableName = 'users';

    public function setUsersTableName($tableName)
    {
        $this->usersTableName = $tableName;
    }

    public function getUsersTableName()
    {
        return $this->usersTableName;
    }


    protected $lockedTableName = 'auth_lock_out';

    public function setLockedTableName($tableName)
    {
        $this->lockedTableName = $tableName;
    }

    public function getLockedTableName()
    {
        return $this->lockedTableName;
    }


    protected $lockTries = 3;

    public function setLockTries($lockTries)
    {
        $this->lockTries = $lockTries;
    }

    public function getLockTries()
    {
        return $this->lockTries;
    }


    protected $waitTime = 30;

    public function setWaitTime($waitTime)
    {
        $this->waitTime = $waitTime;
    }

    public function getWaitTime()
    {
        return $this->waitTime;
    }


    protected $passwordCost = 14;

    public function setPasswordCost($cost)
    {
        $this->passwordCost = $cost;
    }

    public function getPasswordCost()
    {
        return $this->passwordCost;
    }


    protected $passwordMinLength = 8;

    public function setPasswordMinLength($minLength)
    {
        $this->passwordMinLength = $minLength;
    }

    public function getPasswordMinLength()
    {
        return $this->passwordMinLength;
    }


    protected $api = null;

    public function setApi(array $options)
    {
        $this->api = new Api\Options($options);
    }

    /**
     * @return Api\OptionsInterface
     */
    public function getApi() : Api\OptionsInterface
    {
        return $this->api;
    }
}