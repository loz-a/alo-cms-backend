<?php
namespace Auth\Options\Api;

use Zend\Stdlib\AbstractOptions;

class JWTOptions extends AbstractOptions
    implements JWTOptionsInterface
{
    private $aud = null;

    public function setAudience(string $aud)
    {
        $this->aud = $aud;
    }

    public function getAudience()
    {
        return $this->aud;
    }


    private $audFlag = false;

    public function setAudienceReplicateAsHeaderFlag(bool $flag)
    {
        $this->audFlag = $flag;
    }

    public function getAudienceReplicateAsHeaderFlag()
    {
        return $this->audFlag;
    }


    private $exp = null;

    public function setExpiration(int $exp)
    {
        $this->exp = $exp;
    }

    public function getExpiration()
    {
        if (is_null($this->exp)) {
            $this->exp = time() + 3600;
        }

        return $this->exp;
    }


    private $expFlag = false;

    public function setExpirationReplicateAsHeaderFlag(bool $flag)
    {
        $this->expFlag = $flag;
    }

    public function getExpirationReplicateAsHeaderFlag()
    {
        $this->expFlag;
    }


    private $jti = null;

    public function setId(string $jti)
    {
        $this->jti = $jti;
    }

    public function getId()
    {
        return $this->jti;
    }


    private $jtiFlag = true;

    public function setIdReplicateAsHeaderFlag(bool $flag)
    {
        $this->jtiFlag = $flag;
    }

    public function getIdReplicateAsHeaderFlag()
    {
        return $this->jtiFlag;
    }


    private $iat = null;

    public function setIssuedAt(int $iat)
    {
        $this->$iat = $iat;
    }

    public function getIssuedAt()
    {
        if (is_null($this->iat)) {
            $this->iat = time();
        }
        return $this->iat;
    }


    private $iatFlag = false;

    public function setIssuedAtReplicateAsHeaderFlag(bool $flag)
    {
        $this->iatFlag = $flag;
    }

    public function getIssuedAtReplicateAsHeaderFlag()
    {
        return $this->iatFlag;
    }


    private $iss = null;

    public function setIssuer(string $iss)
    {
        $this->iss = $iss;
    }

    public function getIssuer()
    {
        return $this->iss;
    }


    private $issFlag = false;

    public function setIssuerReplicateAsHeaderFlag(bool $flag)
    {
        $this->issFlag = $flag;
    }

    public function getIssuerReplicateAsHeaderFlag()
    {
        return $this->issFlag;
    }


    private $nbf = null;

    public function setNotBefore(int $nbf)
    {
        $this->nbf = $nbf;
    }

    public function getNotBefore()
    {
        if (is_null($this->nbf)) {
            $this->nbf = time();
        }
        return $this->nbf;
    }


    private $nbfFlag = false;

    public function setNotBeforeReplicateAsHeaderFlag(bool $flag)
    {
        $this->nbfFlag = $flag;
    }

    public function getNotBeforeReplicateAsHeaderFlag()
    {
        return $this->nbfFlag;
    }


    private $sub = null;

    public function setSubject(string $sub)
    {
        $this->sub = $sub;
    }

    public function getSubject()
    {
        return $this->sub;
    }


    private $subFlag = false;

    public function setSubjectReplicateAsHeaderFlag(bool $flag)
    {
        $this->subFlag = $flag;
    }

    public function getSubjectReplicateAsHeaderFlag()
    {
        return $this->subFlag;
    }


    private $signerClass = 'Lcobucci\JWT\Signer\Hmac\Sha256';

    public function setSignerClass(string $signerClass)
    {
        $this->signerClass = $signerClass;
    }

    public function getSignerClass()
    {
        return $this->signerClass;
    }


    private $secret = '';

    public function setSecret(string $secret)
    {
        $this->secret = $secret;
    } // setSecret()


    public function getSecret()
    {
        return $this->secret;
    } // getSecret()

}