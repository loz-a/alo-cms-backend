<?php
namespace Auth\Options\Api;


interface JWTOptionsInterface
{
    public function setAudience(string $aud);
    public function getAudience();

    public function setAudienceReplicateAsHeaderFlag(bool $flag);
    public function getAudienceReplicateAsHeaderFlag();

    public function setExpiration(int $exp);
    public function getExpiration();

    public function setExpirationReplicateAsHeaderFlag(bool $flag);
    public function getExpirationReplicateAsHeaderFlag();

    public function setId(string $jti);
    public function getId();

    public function setIdReplicateAsHeaderFlag(bool $flag);
    public function getIdReplicateAsHeaderFlag();

    public function setIssuedAt(int $iat);
    public function getIssuedAt();

    public function setIssuedAtReplicateAsHeaderFlag(bool $flag);
    public function getIssuedAtReplicateAsHeaderFlag();

    public function setIssuer(string $iss);
    public function getIssuer();

    public function setIssuerReplicateAsHeaderFlag(bool $flag);
    public function getIssuerReplicateAsHeaderFlag();

    public function setNotBefore(int $nbf);
    public function getNotBefore();

    public function setNotBeforeReplicateAsHeaderFlag(bool $flag);
    public function getNotBeforeReplicateAsHeaderFlag();

    public function setSubject(string $sub);
    public function getSubject();

    public function setSubjectReplicateAsHeaderFlag(bool $flag);
    public function getSubjectReplicateAsHeaderFlag();

    public function setSignerClass(string $signerClass);
    public function getSignerClass();

    public function setSecret(string $secret);
    public function getSecret();
}