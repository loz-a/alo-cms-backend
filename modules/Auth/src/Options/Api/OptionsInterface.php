<?php
namespace Auth\Options\Api;


interface OptionsInterface
{
    public function setJwtOptions(array $jwtOptions);
    public function getJwtOptions() : JWTOptionsInterface;
}