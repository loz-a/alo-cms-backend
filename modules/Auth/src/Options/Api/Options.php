<?php
namespace Auth\Options\Api;

use Zend\Stdlib\AbstractOptions;

class Options extends AbstractOptions
    implements OptionsInterface
{

    /**
     * @var JWTOptionsInterface
     */
    private $jwtOptions;

    public function setJwtOptions(array $jwtOptions = [])
    {
        $this->jwtOptions = new JWTOptions($jwtOptions);
    }

    public function getJwtOptions() : JWTOptionsInterface
    {
        if (!$this->jwtOptions) {
            $this->setJwtOptions();
        }

        return $this->jwtOptions;
    }

}