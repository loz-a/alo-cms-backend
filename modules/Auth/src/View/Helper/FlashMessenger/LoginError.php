<?php
namespace Auth\View\Helper\FlashMessenger;

use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Zend\View\Helper\AbstractHelper;

class LoginError extends AbstractHelper
{
    /**
     * @var FlashMessenger
     */
    private $flashMessenger;

    public function __construct(FlashMessenger $flashMessenger)
    {
        $this->flashMessenger = $flashMessenger;
    } // __construct()

    public function __invoke()
    {
        $values = [];
        $fm = $this->flashMessenger;

        if ($fm->hasCurrentErrorMessages()) {
            $values['error'] = current($fm->getCurrentErrorMessages());
        }

        return $this->getView()->render('auth::login-error', $values);
    } // __invoke()
}