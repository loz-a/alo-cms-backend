<?php
namespace Auth\View\Helper\FlashMessenger;

use Interop\Container\ContainerInterface;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class LoginErrorFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $fm = $container->get(FlashMessenger::class);
        return new LoginError($fm);
    }
}