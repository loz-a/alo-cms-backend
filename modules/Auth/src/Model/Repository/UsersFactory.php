<?php
namespace Auth\Model\Repository;

use Interop\Container\ContainerInterface;
use Auth\Model\Storage\UsersInterface as UsersStorageInterface;

class UsersFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $storage = $container->get(UsersStorageInterface::class);
        return new Users($storage);
    } // __invoke()
}