<?php
namespace Auth\Model\Repository;

use Interop\Container\ContainerInterface;
use Auth\Model\Storage\LockoutInterface as LockoutStorageInterface;

class LockoutFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $storage = $container->get(LockoutStorageInterface::class);
        return new Lockout($storage);
    } // __invoke()
}