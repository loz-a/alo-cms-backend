<?php
namespace Auth\Model\Repository;

use Auth\Model\Entity\BlockedUser;

interface LockoutInterface
{
    public function findByLogin(string $login);

    public function insert(BlockedUser $blockedUser);

    public function update(BlockedUser $blockedUser);

    public function delete(BlockedUser $blockedUser);
}