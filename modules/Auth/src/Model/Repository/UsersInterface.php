<?php
namespace Auth\Model\Repository;

use Auth\Model\Entity\User;

interface UsersInterface
{
    public function findUserById(int $id) : User;

    public function findUserByLogin(string $login) : User;

    public function updateLastLogged(User $user);
}