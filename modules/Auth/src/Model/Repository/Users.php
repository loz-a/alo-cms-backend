<?php
namespace Auth\Model\Repository;

use Auth\Model\Entity\User;
use Auth\Model\Storage\UsersInterface as UsersStorageInterface;

class Users implements UsersInterface
{
    /**
     * @var UsersStorageInterface
     */
    protected $usersStorage;

    public function __construct(UsersStorageInterface $usersStorage)
    {
        $this->usersStorage = $usersStorage;
    } // __construct()


    public function findUserById(int $id) : User
    {
        return $this->usersStorage->findUserById($id);
    }


    public function findUserByLogin(string $login) : User
    {
        return $this->usersStorage->findUserByLogin($login);
    }


    public function updateLastLogged(User $user)
    {
        return $this->usersStorage->updateLastLogged($user);
    }


}