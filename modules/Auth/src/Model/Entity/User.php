<?php
namespace Auth\Model\Entity;

class User
{
    protected $id;

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }


    protected $login;

    public function setLogin(string $login)
    {
        $this->login = $login;
        return $this;
    }

    public function getLogin()
    {
        return $this->login;
    }


    protected $password;

    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }


    protected $lastLogged;

    public function setLastLogged(int $lastLogged)
    {
        $this->lastLogged = $lastLogged;
        return $this;
    }

    public function getLastLogged()
    {
        return $this->lastLogged;
    }

} // User