<?php
namespace Auth\Model\Entity;


class BlockedUser
{
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }


    protected $lockTime;

    public function getLockTime()
    {
        return $this->lockTime;
    }

    public function setLockTime(int $lockTime)
    {
        $this->lockTime = $lockTime;
        return $this;
    }


    protected $lockTries;

    public function getLockTries()
    {
        return $this->lockTries;
    }

    public function setLockTries(int $lockTries)
    {
        $this->lockTries = $lockTries;
        return $this;
    }

}