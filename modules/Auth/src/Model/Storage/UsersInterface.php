<?php
namespace Auth\Model\Storage;

use Auth\Model\Repository\UsersInterface as BaseUserInterface;

interface UsersInterface extends BaseUserInterface
{
}