<?php
namespace Auth\Model\Storage\Db;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface as DbAdapterInterface;
use Auth\Options\ModuleOptions as AuthOptions;

class LockoutFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authOptions = $container->get(AuthOptions::class);
        $table       = $authOptions->getLockedTableName();
        $usersTable  = $authOptions->getUsersTableName();
        $dbAdapter   = $container->get(DbAdapterInterface::class);

        return new Lockout($table, $usersTable, $dbAdapter);
    } // __invoke()
}