<?php
namespace Auth\Model\Storage\Db;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface as DbAdapterInterface;
use Auth\Options\ModuleOptions as AuthOptions;

class UsersFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authOptions = $container->get(AuthOptions::class);
        $table       = $authOptions->getUsersTableName();
        $dbAdapter   = $container->get(DbAdapterInterface::class);

        return new Users($table, $dbAdapter);
    } // __invoke()
}