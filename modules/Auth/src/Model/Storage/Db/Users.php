<?php
namespace Auth\Model\Storage\Db;

use App\Domain\Storage\Db\AbstractDbStorage;
use Auth\Model\Entity\User;
use Auth\Model\Storage\UsersInterface;
use Zend\Db\Sql\Sql;

class Users extends AbstractDbStorage
    implements UsersInterface
{
    /**
     * @param $id
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function findUserById(int $id) : User
    {
        $sql       = new Sql($this->dbAdapter);
        $select    = $sql->select($this->table)->where(['id' => $id]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute($select);

        return $result->count() ? $this->hydrateUser($result->current()) : null;
    }


    public function findUserByLogin(string $login) : User
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql
            ->select($this->table)
            ->where(['login' => $login])
            ->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute($select);

        return $result->count() ? $this->hydrateUser($result->current()) : null;
    }


    public function updateLastLogged(User $user)
    {
        $sql = new Sql($this->dbAdapter);

        $update = $sql->update($this->table);
        $update->set(['last_logged' => $user->getLastLogged()]);
        $update->where(['id' => $user->getId()]);

        return (bool) $sql->prepareStatementForSqlObject($update)->execute();
    }


    protected function hydrateUser($userData)
    {
        $user = new User();
        $user->setId($userData['id']);
        $user->setLogin($userData['login']);
        $user->setPassword($userData['password']);

        return $user;
    }
}