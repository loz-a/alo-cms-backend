<?php
namespace Auth\Middleware;

use Auth\Service\UserIdentity;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\I18n\Translator\TranslatorInterface;

class LoginRequiredFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userIdentity = $container->get(UserIdentity::class);
        $router       = $container->get(RouterInterface::class);
        $translator   = $container->get(TranslatorInterface::class);
        return new LoginRequired($userIdentity, $router, $translator);
    } // __invoke()
}