<?php
namespace Auth\Middleware\Api;

use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;
use Auth\Options\ModuleOptions;
use Auth\Responder\Api\Unauthorized as UnauthorizedResponder;
use Interop\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;
use Zend\Expressive\Router\RouterInterface;

class TokenVerifyFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $jwtOptions = $container->get(ModuleOptions::class)->getApi()->getJwtOptions();
        $userRepository = $container->get(UsersRepositoryInterface::class);
        $translator = $container->get(TranslatorInterface::class);
        $router = $container->get(RouterInterface::class);
        $responder  = new UnauthorizedResponder();

        return new TokenVerify(
            $jwtOptions,
            $userRepository,
            $translator,
            $router,
            $responder
        );
    }
}