<?php
namespace Auth\Middleware\Api;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Token;
use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;
use Auth\Options\Api\JWTOptionsInterface;
use Auth\Responder\Api\Unauthorized as UnauthorizedResponder;
use Zend\Code\Reflection\ClassReflection;
use Zend\I18n\Translator\TranslatorInterface;
use Auth\Translator\TranslatorTrait;
use Throwable;
use Zend\Expressive\Router\RouteResult;
use Zend\Expressive\Router\RouterInterface;

class TokenVerify
{
    use TranslatorTrait;

    const TOKEN_VERIFY_TAG = 'Api.TokenVerify';

    /**
     * @var JWTOptionsInterface
     */
    private $jwtOptions;

    /**
     * @var UsersRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UnauthorizedResponder
     */
    private $responder;

    public function __construct(
        JWTOptionsInterface $jwtOptions,
        UsersRepositoryInterface $userRepository,
        TranslatorInterface $translator,
        RouterInterface $router,
        UnauthorizedResponder $responder
    ){
        $this->jwtOptions = $jwtOptions;
        $this->userRepository = $userRepository;
        $this->translator = $translator;
        $this->router = $router;
        $this->responder = $responder;
    } // __construct()


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $routeResult = $request->getAttribute(RouteResult::class, false);

        if (!$routeResult) {
            return $next($request, $response);
        }

        $middlewares = $routeResult->getMatchedMiddleware();

        if (!$this->hasProtectedMiddleware($middlewares)) {
            return $next($request, $response);
        }

        if (false === ($strToken = $this->getRequestToken($request))) {
            return $this->errorUnauthorized();
        }

        try {
            $token = (new Parser())->parse($strToken);
        } catch (Throwable $t) {
            return $this->errorUnauthorized();
        }

        if (!$this->validate($token, $this->jwtOptions)
            || $token->isExpired()
        ){
            return $this->errorUnauthorized();
        }

        if (!$this->verify($token, $this->jwtOptions)) {
            return $this->errorUnauthorized();
        }

        $request = $request->withAttribute('jwtToken', $token);

        return $next($request, $response);
    } // __invoke()


    protected function getRequestToken(ServerRequestInterface $request)
    {
        if (false !== ($token = $this->getTokenFromHeaders($request))) {
            return $token;
        }

        if (false !== ($token = $request->getCookieParams()['token'] ?? false)) {
            return $token;
        }

        return $request->getQueryParams()['token'] ?? false;
    }


    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    protected function getTokenFromHeaders(ServerRequestInterface $request)
    {
        $params = $request->getServerParams();

        /* Check for each given environment */
        $header = $params['HTTP_AUTHORIZATION'] ?? '';

        /* Nothing in environment, try header instead */
        if (empty($header)) {
            $header = $request->getHeader('Authorization')[0] ?? '';
        }

        /* Try apache_request_headers() as last resort */
        if (empty($header) && function_exists('apache_request_headers')) {
            $header = apache_request_headers()['Authorization'] ?? '';
        }

        list($token) = sscanf($header, 'Bearer %s');

        return $token ? $token : false;
    }


    protected function validate(Token $token, JWTOptionsInterface $jwtOptions)
    {
        $data = new ValidationData();

        if (null !== ($aud = $jwtOptions->getAudience())) {
            $data->setAudience($aud);
        }

        if (null !== ($jti = $jwtOptions->getId())) {
            $data->setId($jti);
        }

        if (null !== ($iss = $jwtOptions->getIssuer())) {
            $data->setIssuer($iss);
        }

        if (null !== ($sub = $jwtOptions->getSubject())) {
            $data->setSubject($sub);
        }

        return $token->validate($data);
    }


    protected function verify(Token $token, JWTOptionsInterface $jwtOptions)
    {
        if (null === ($data = $token->getClaim('data'))
            || empty($data->login)
            || null === ($user = $this->userRepository->findUserByLogin($data->login))
        ){
            return false;
        }

        $SignerClass = $jwtOptions->getSignerClass();

        if (!class_exists($SignerClass)) {
            throw new Exception\ClassNotExistsException(sprintf('Class %s not exists', $SignerClass));
        }

        $signer = new $SignerClass();
        return $token->verify($signer, $jwtOptions->getSecret());
    }


    protected function errorUnauthorized()
    {
        return $this->responder->__invoke([
            'data' => [
                'error' => $this->translate('Invalid token'),
                'loginUri' => $this->router->generateUri('login')
            ]
        ]);
    }


    protected function hasProtectedMiddleware($middlewares)
    {
        $result = false;

        if (is_string($middlewares)) {
            $middlewares = [$middlewares];
        }

        if (!is_array($middlewares)) {
            return $result;
        }

        foreach ($middlewares as $middleware) {
            $docBlock = (new ClassReflection($middleware))->getDocBlock();
            if ($docBlock && $docBlock->hasTag(self::TOKEN_VERIFY_TAG)) {
                $result = true;
                break;
            }

        }

        return $result;
    }
}