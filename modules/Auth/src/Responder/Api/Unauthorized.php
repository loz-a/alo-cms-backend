<?php
namespace Auth\Responder\Api;

class Unauthorized extends Login
{
    const UNAUTHORIZED_CODE = 401;

    public function __invoke(array $data = [])
    {
        $data['response_status'] = self::UNAUTHORIZED_CODE;

        return $this->getResponse($data);
    } // __invoke()
}