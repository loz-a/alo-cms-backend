<?php
namespace Auth\Responder\Api;

use App\Responder\JsonResponder;
use Auth\Options\Api\CorsHeadersInterface;

class Login extends JsonResponder
{
    protected $corsHeaders = [];

    public function __invoke(array $data = [])
    {
        if (empty($data['data']['token'])) {
            $data['response_status'] = 401;
        }

        return $this->getResponse($data);
    } // __invoke()

}