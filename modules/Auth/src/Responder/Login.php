<?php
namespace Auth\Responder;

use App\Responder\HtmlResponder;
use Auth\ConfigProvider;
use Auth\Form\Login as LoginForm;

class Login extends HtmlResponder
{
    public function __invoke(LoginForm $loginForm)
    {
        $data = [
            'template' => 'auth::login',
            'layout' => 'layout::auth',
            'template_params' => [
                'form'   => $loginForm,
                'translationTextDomain' => ConfigProvider::MODULE_NAME
            ]
        ];

        return $this->getResponse($data);
    } // __invoke()
}