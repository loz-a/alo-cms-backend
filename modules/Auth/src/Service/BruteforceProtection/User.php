<?php
namespace Auth\Service\BruteforceProtection;

use Auth\Model\Entity\BlockedUser;
use Auth\Model\Repository\LockoutInterface as LockoutRepositoryInterface;
use Auth\Options\AuthInterface as AuthOptionsInterface;

/**
 * Class User
 * @package Auth\Service\BruteforceProtection
 * @todo make optionally available or not in module options
 */
class User
{
    /**
     * @var AuthOptionsInterface
     */
    protected $options;

    /**
     * @var LockoutRepositoryInterface
     */
    protected $lockoutRepository;

    public function __construct(
        AuthOptionsInterface $options,
        LockoutRepositoryInterface $lockoutRepository
    ){
        $this->options = $options;
        $this->lockoutRepository = $lockoutRepository;

    } // __construct()


    public function isLocked(string $identity) : bool
    {
        $lockTries = $this->options->getLockTries();
        $waitTime  = $this->options->getWaitTime();

        $lockedUser = $this->lockoutRepository->findByLogin($identity);

        if (!$lockedUser) {
            return false;
        }

        $timeNotExpired = time() <= ($lockedUser->getLockTime() + $waitTime);
        $triesExhausted = $lockedUser->getLockTries() > $lockTries;

        if (!$timeNotExpired && $lockedUser->getLockTries() > 0) {
            $this->lockoutRepository->delete($lockedUser);
        }
        else if ($timeNotExpired && $triesExhausted) {
            $this->increaseTriesCounter($lockedUser);
            return true;
        }
        return false;
    } // isLocked()


    public function increaseTriesCounter($lockedUser)
    {
        $repository = $this->lockoutRepository;

        if (!$lockedUser instanceof BlockedUser) {
            $lockedUser = $repository->findByLogin(strval($lockedUser));
        }

        if (!$lockedUser) {
            return;
        }

        $lockTries = $lockedUser->getLockTries();

        $lockedUser->setLockTime(time());
        if (!$lockTries) {
            $lockedUser->setLockTries(1);
            $repository->insert($lockedUser);
        }
        else {
            $lockedUser->setLockTries($lockTries + 1);
            $repository->update($lockedUser);
        }
    } // increaseTriesCounter()
}