<?php
namespace Auth\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result as AuthResult;

class Auth
{
    /**
     * @var AuthenticationService
     */
    protected $authService;


    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    } // __construct()


    /**
     * @param $login
     * @param $password
     * @return AuthResult
     */
    public function login($login, $password) : AuthResult
    {
        $adapter = $this->authService->getAdapter();
        $adapter
            -> setIdentity($login)
            -> setCredential($password);

        return $this->authService->authenticate();
    } // login()


    public function logout()
    {
        $this->authService->clearIdentity();
    } // logout()
}