<?php
namespace Auth\Service\Jwt\Exception;

use LogicException;

class ClassNotExistsException extends LogicException
{

}