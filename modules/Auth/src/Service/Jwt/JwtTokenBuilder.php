<?php
namespace Auth\Service\Jwt;

use Auth\Options\Api\JWTOptionsInterface;
use Lcobucci\JWT\Builder;

class JwtTokenBuilder
{
    /**
     * @param JWTOptionsInterface $jwtOptions
     * @param array $data
     * @return \Lcobucci\JWT\Token
     */
    public static function build(JWTOptionsInterface $jwtOptions, array $data)
    {
        $builder = self::hydrate($jwtOptions, new Builder());

        $SignerClass = $jwtOptions->getSignerClass();

        if (!class_exists($SignerClass)) {
            throw new Exception\ClassNotExistsException(sprintf('Class %s not exists', $SignerClass));
        }

        $signer = new $SignerClass();

        return $builder
            ->set('data', $data)
            ->sign($signer, $jwtOptions->getSecret())
            ->getToken();
    } // build()


    public static function hydrate(JWTOptionsInterface $jwtOptions, $object)
    {
        if (null !== ($aud = $jwtOptions->getAudience())) {
            $object->setAudience($aud, $jwtOptions->getAudienceReplicateAsHeaderFlag());
        }

        if (null !== ($exp = $jwtOptions->getExpiration())) {
            $object->setExpiration($exp, $jwtOptions->getExpirationReplicateAsHeaderFlag());
        }

        if (null !== ($jti = $jwtOptions->getId())) {
            $object->setId($jti, $jwtOptions->getIdReplicateAsHeaderFlag());
        }

        if (null !== ($iat = $jwtOptions->getIssuedAt())) {
            $object->setIssuedAt($iat, $jwtOptions->getIssuedAtReplicateAsHeaderFlag());
        }

        if (null !== ($iss = $jwtOptions->getIssuer())) {
            $object->setIssuer($iss, $jwtOptions->getIssuerReplicateAsHeaderFlag());
        }

        if (null !== ($nbf = $jwtOptions->getNotBefore())) {
            $object->setNotBefore($nbf, $jwtOptions->getNotBeforeReplicateAsHeaderFlag());
        }

        if (null !== ($sub = $jwtOptions->getSubject())) {
            $object->setSubject($sub, $jwtOptions->getSubjectReplicateAsHeaderFlag());
        }

        return $object;
    }
}