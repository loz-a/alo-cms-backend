<?php
namespace Auth\Service;

use Auth\Service\Adapter\Password\Bcrypt;
use Interop\Container\ContainerInterface;
use Auth\Options\ModuleOptions as AuthOptions;
use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;

class UserFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authOptions      = $container->get(AuthOptions::class);
        $userRepository   = $container->get(UsersRepositoryInterface::class);
        $userIdentity     = $container->get(UserIdentity::class);

        $passwordStrategy = new Bcrypt();
        $passwordStrategy->setCost($authOptions->getPasswordCost());

        return new User(
            $authOptions,
            $userRepository,
            $userIdentity,
            $passwordStrategy
        );
    } // __invoke()
}