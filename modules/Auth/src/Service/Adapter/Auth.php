<?php
namespace Auth\Service\Adapter;

use Auth\Model\Repository\Users as UsersRepository;
use Zend\Authentication\Result;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class Auth implements AuthInterface, EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    /**
     * @var UsersRepository
     */
    protected $usersRepository;

    /**
     * @var Password\StrategyInterface
     */
    protected $passwordStrategy;

    /**
     * @var string
     */
    protected $identity;

    /**
     * @var string
     */
    protected $credential;

    public function __construct(
        UsersRepository $usersRepository,
        Password\StrategyInterface $passwordStrategy
    ){
        $this->usersRepository  = $usersRepository;
        $this->passwordStrategy = $passwordStrategy;
    } // __construct()

    public function authenticate()
    {
        $identity   = $this->getIdentity();
        $credential = $this->getCredential();

        $user = $this->usersRepository->findUserByLogin($identity);

        if ($user) {
            $result = $this->passwordStrategy->password()->verify($credential, $user->getPassword());

            if ($result) {
                $messages = ['Authentication successful'];
                $authResult = new Result(Result::SUCCESS, $identity, $messages);

                $event = $this->createEvent($user, $authResult);
                $this->getEventManager()->triggerEvent($event);

                return $authResult;
            }
        }

        $messages = ['Supplied credential is invalid'];
        return new Result(Result::FAILURE, $identity, $messages);
    }


    /**
     * setIdentity() - set the value to be used as the identity
     *
     * @param  string $value
     * @return $this
     */
    public function setIdentity($value)
    {
        $this->identity = $value;
        return $this;
    } // setIdentity()


    public function getIdentity()
    {
        if (null === $this->identity) {
            throw new Exception\LogicException(
                'A value for the identity was not provided prior to authentication with database');
        }
        return $this->identity;
    } // getIdentity()


    /**
     * setCredential() - set the credential value to be used, optionally can specify a treatment
     * to be used, should be supplied in parametrized form, such as 'MD5(?)' or 'PASSWORD(?)'
     *
     * @param  string $credential
     * @return $this
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;
        return $this;
    } // setCredential()


    public function getCredential()
    {
        if (null === $this->credential) {
            throw new Exception\LogicException('A credential value was not provided prior to authentication');
        }
        return $this->credential;
    } // getCredential()


    protected function createEvent($user, $authenticationResult)
    {
        $event = new Event();
        $event->setName('lastLogged.update');
        $event->setTarget($this);
        $event->setUser($user);
        $event->setAuthenticateResult($authenticationResult);
        return $event;
    } // createEvent()

}