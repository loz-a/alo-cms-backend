<?php
namespace Auth\Service\Adapter;

use Interop\Container\ContainerInterface;
use Auth\Options\ModuleOptions;
use Auth\Model\Repository\UsersInterface;
use Auth\Service\Adapter\Listener\LastLogged as LastLoggedListener;

class AuthFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $usersRepository = $container->get(UsersInterface::class);
        $authOptions     = $container->get(ModuleOptions::class);

        $passwordStrategy  = new Password\Bcrypt();
        $passwordStrategy->setCost($authOptions->getPasswordCost());

        $authService = new Auth($usersRepository, $passwordStrategy);

        $lastLoggedListener = new LastLoggedListener($usersRepository);
        $lastLoggedListener->attach($authService->getEventManager());

        return $authService;
    } // __invoke()
}