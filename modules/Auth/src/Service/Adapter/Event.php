<?php
namespace Auth\Service\Adapter;

use Zend\EventManager\Event as BaseEvent;
use Auth\Model\Entity\User;
use Zend\Authentication\Result as AuthenticateResult;

class Event extends BaseEvent
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var AuthenticateResult
     */
    protected $authenticateResult;

    /**
     * @return AuthenticateResult
     */
    public function getAuthenticateResult()
    {
        return $this->authenticateResult;
    }

    /**
     * @param AuthenticateResult $authenticateResult
     * @return $this
     */
    public function setAuthenticateResult(AuthenticateResult $authenticateResult)
    {
        $this->authenticateResult = $authenticateResult;
        return $this;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }


}