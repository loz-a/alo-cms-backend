<?php
namespace Auth\Service\Adapter\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Auth\Model\Repository\UsersInterface as UsersRepositoryInterface;
use Auth\Service\Adapter\Event;

class LastLogged implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var UsersRepositoryInterface
     */
    protected $userRepository;

    public function __construct(
        UsersRepositoryInterface $userRepository
    ){
        $this->userRepository = $userRepository;
    }


    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach('lastLogged.update', [$this, 'update']);
    }


    public function update(Event $e)
    {
        $user = $e->getUser();
        $user->setLastLogged(time());
        $this->userRepository->updateLastLogged($user);
    }

}