<?php
namespace Auth\Service\Adapter\Password;

use Zend\Crypt\Password\PasswordInterface as ZendPasswordInterface;

interface StrategyInterface
{
    public function password() : ZendPasswordInterface;
}