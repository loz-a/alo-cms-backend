<?php
namespace Auth\Mapper;

use Core\AppTestCase;
use Core\AbstractDbMapper;
use Faker\Factory as FakerFactory;
use Auth\Mapper\Users as UsersMapper;

class UsersTest extends AppTestCase
{
    public function setUp()
    {
        parent::setUp();
        AbstractDbMapper::configure('sqlite:./data/database/test_database.sqlite');
        AbstractDbMapper::configure('return_result_sets', true);
        AbstractDbMapper::get_db() -> beginTransaction();
    } // setUp()


    public function tearDown()
    {
        AbstractDbMapper::get_db() -> rollBack();
    } // tearDown()()


    public function testFindOneByIdentity()
    {
        $mapper = new UsersMapper($this -> app);
        $faker = FakerFactory::create();

        $expectedIdentity = $faker -> userName;
        $table = $mapper -> getTable();

        for ($i = 0; $i < 3; $i++) {
            $user = $table -> create();
            $user -> login = ($i === 1) ? $expectedIdentity : $faker -> userName;
            $user -> password = $faker -> md5;
            $user -> save();
        }

        $user = $mapper -> findOneByIdentity($expectedIdentity);
        $this -> assertEquals($expectedIdentity, $user -> login);
    }


    public function testInsertUnsaccessfulData()
    {
        $mapper = new UsersMapper($this -> app);

        $data = [
            'id' => 1,
            'attempt_time' => 1371216619,
            'count' => 1
        ];
        $mapper -> insertUnsuccessfulData($data);

        $tableName = $tableName = $this -> app -> getOptions('auth', 'unsuccessful_login_table_name');
        $results = UsersMapper::for_table($tableName)
            -> where('attempt_time', $data['attempt_time'])
            -> find_many();

        $this -> assertCount(1, $results);
    } // testInsertUnsaccessfulData()


    public function testFindUnsuccessfulLogDataById()
    {
        $mapper = new UsersMapper($this -> app);
        $faker = FakerFactory::create();

        $testId = 2;
        $insertData = [];
        for ($i = 1; $i <= 3; $i++) {
            $insertData[$i] = [
                'id' => $i,
                'attempt_time' => $faker -> unixTime,
                'count' => $faker -> randomDigit
            ];
            $mapper -> insertUnsuccessfulData($insertData[$i]);
        }
        $result = $mapper -> findUnsuccessfulLogDataById($testId);
        $this -> assertEquals($insertData[$testId], $result -> as_array());
    }


    public function testUpdateExistsUnsuccessfulLogDataById()
    {
        $mapper = new UsersMapper($this -> app);
        $faker = FakerFactory::create();

        $testId = 1;
        $mapper -> insertUnsuccessfulData([
            'id' => $testId,
            'attempt_time' => $faker -> unixTime,
            'count' => $faker -> randomDigit
        ]);

        $newData = [
            'attempt_time' => $faker -> unixTime,
            'count' => $faker -> randomDigit
        ];
        $mapper -> updateUnsuccessfulDataById($newData, $testId);
        $newData['id'] = $testId;

        $result = $mapper -> findUnsuccessfulLogDataById($testId);
        $this -> assertEquals($newData, $result -> as_array());
    } // testUpdateUnsuccessfulLogDataById()


    public function testUpdateIfNotExistsUnsuccessfulLogDataById()
    {
        $mapper = new UsersMapper($this -> app);
        $faker = FakerFactory::create();
        $recordId = 1;

        $mapper -> updateUnsuccessfulDataById([
            'attempt_time' => $faker -> unixTime,
            'count' => $faker -> randomDigit
        ], $recordId);

        $result = $mapper -> findUnsuccessfulLogDataById($recordId);
        $this -> assertEquals($result -> id, $recordId);
    }
}
