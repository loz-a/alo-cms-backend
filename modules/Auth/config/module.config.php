<?php

use Auth\ConfigProvider;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;

return [
    'dependencies' => [

        'factories' => [
            BodyParamsMiddleware::class => InvokableFactory::class,

            Auth\Action\Config::class => Auth\Action\ConfigFactory::class,

            Auth\Action\UserBruteforceProtection::class => Auth\Action\UserBruteforceProtectionFactory::class,
            Auth\Action\Login::class => Auth\Action\LoginFactory::class,
            Auth\Action\Logout::class => Auth\Action\LogoutFactory::class,

            Auth\Action\Api\UserBruteforceProtection::class => Auth\Action\Api\UserBruteforceProtectionFactory::class,
            Auth\Action\Api\Login::class => Auth\Action\Api\LoginFactory::class,
            Auth\Action\Api\Logout::class => Auth\Action\Api\LogoutFactory::class,
            Auth\Action\Api\JWTtoken::class => Auth\Action\Api\JWTtokenFactory::class,

            Auth\Options\ModuleOptions::class => Auth\Options\Factory::class,
            Auth\Form\Login::class => Auth\Form\LoginFactory::class,
            Auth\Form\InputFilter\Login::class => InvokableFactory::class,

            Auth\Model\Storage\UsersInterface::class => Auth\Model\Storage\Db\UsersFactory::class,
            Auth\Model\Repository\UsersInterface::class => Auth\Model\Repository\UsersFactory::class,

            Auth\Model\Storage\LockoutInterface::class => Auth\Model\Storage\Db\LockoutFactory::class,
            Auth\Model\Repository\LockoutInterface::class => Auth\Model\Repository\LockoutFactory::class,

            Auth\Service\User::class => Auth\Service\UserFactory::class,
            Auth\Service\UserIdentity::class => Auth\Service\UserIdentityFactory::class,
            Auth\Service\Adapter\AuthInterface::class => Auth\Service\Adapter\AuthFactory::class,
            Auth\Service\Auth::class => Auth\Service\AuthFactory::class,
            Zend\Authentication\AuthenticationServiceInterface::class => Auth\Service\ZendAuthFactory::class,
            Auth\Service\BruteforceProtection\User::class => Auth\Service\BruteforceProtection\UserFactory::class,

            Auth\Middleware\LoginRequired::class => Auth\Middleware\LoginRequiredFactory::class,
            Auth\Middleware\Api\TokenVerify::class => Auth\Middleware\Api\TokenVerifyFactory::class,
        ],
    ],


    'routes' => [
        [   'name' => 'api_login',
            'path' => '/api/login',
            'middleware' => [
                BodyParamsMiddleware::class,
                Auth\Action\Api\UserBruteforceProtection::class,
                Auth\Action\Api\Login::class,
                Auth\Action\Api\JWTtoken::class,
            ],
            'allowed_methods' => ['POST']
        ],
        [   'name' => 'api_logout',
            'path' => '/api/logout',
            'middleware' => [
                Auth\Action\Api\Logout::class
            ],
            'allowed_methods' => ['GET']
        ],
        [   'name' => 'login',
            'path' => '/login',
            'middleware' => [
                Auth\Action\UserBruteforceProtection::class,
                Auth\Action\Login::class,
            ],
            'allowed_methods' => ['GET', 'POST']
        ],
        [   'name' => 'logout',
            'path' => '/logout',
            'middleware' => Auth\Action\Logout::class,
            'allowed_methods' => ['GET']
        ],
        [   'name' => 'auth_config',
            'path' => '/auth-config.json',
            'middleware' => Auth\Action\Config::class,
            'allowed_methods' => ['GET']
        ]
    ],


    'templates' => [
        'map' => [
            'layout::auth' => __DIR__ . '/../templates/layout/auth.phtml',
            'auth::login'  => __DIR__ . '/../templates/auth/login.phtml',
            'auth::login-error' => __DIR__ . '/../templates/auth/login-error.phtml',
        ],
        'paths' => [
            'auth'    => [__DIR__ . '/../templates/auth'],
            'layout' => [__DIR__ . '/../templates/layout'],
        ],
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../data/languages',
                'pattern' => '%s.mo',
                'text_domain' => ConfigProvider::MODULE_NAME
            ]
        ]
    ],

    'view_helpers' => [
        'aliases' => [
            'loginError' => Auth\View\Helper\FlashMessenger\LoginError::class,
        ],
        'factories' => [
            Auth\View\Helper\FlashMessenger\LoginError::class => Auth\View\Helper\FlashMessenger\LoginErrorFactory::class,
        ]
    ],

    'admin_navigation' => [
        'logout' => [
            'title' => translate('Sign Out'),
            'icon'  => 'sign out'
        ]
    ]

];