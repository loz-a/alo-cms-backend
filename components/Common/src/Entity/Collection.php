<?php
namespace Common\Entity;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;


abstract class Collection implements  ArrayAccess, IteratorAggregate
{
    protected $elements = [];

    public function offsetExists($offset)
    {
        return isset($this->elements[$offset]);
    }

    public function offsetGet($offset)
    {
        $result = null;
        if ($this->offsetExists($offset)) {
            $result =& $this->elements[$offset];
        }

        return $result;
    }

    public function offsetUnset($offset)
    {
        if (!$this->offsetExists($offset)) {
            return null;
        }

        $removed = $this->elements[$offset];
        unset($this->elements[$offset]);

        return $removed;
    }


    public function toArray()
    {
        return $this->elements;
    }


    public function count()
    {
        return count($this->elements);
    }


    public function getIterator()
    {
        return new ArrayIterator($this->elements);
    }


    public function clear()
    {
        return $this->elements = [];
    }
}