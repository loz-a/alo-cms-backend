<?php
namespace Components\Validator;

use Zend\Validator\AbstractValidator;

class CheckboxValue extends AbstractValidator
{
    const ERROR_INVALID_VALUE = 'invalidValue';

    protected $messageTemplates = [
        self::ERROR_INVALID_VALUE => "Invalid value '%value%'. 1 or 0 values expected"
    ];


    public function isValid($value)
    {
        $this->setValue($value);

        if (is_numeric($value)) {
            $value = (int) $value;

            if ($value === 1 or $value === 0) {
                return true;
            }

        }

        $this->error(self::ERROR_INVALID_VALUE);
        return false;
    }
}