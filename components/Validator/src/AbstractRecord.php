<?php
namespace Components\Validator;

use Zend\Validator\AbstractValidator;

abstract class AbstractRecord extends AbstractValidator
{
    /**
     * Error constants
     */
    const ERROR_NO_RECORD_FOUND = 'noRecordFound';
    const ERROR_RECORD_FOUND    = 'recordFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_NO_RECORD_FOUND => "No record matching '%value%' was found",
        self::ERROR_RECORD_FOUND    => "A record matching '%value%' was found",
    );

    /**
     * @var mixed
     */
    protected $repository;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $exclude;

    /**
     * @param array $options
     */
    public function __construct($options = null)
    {
        if (is_array($options)) {
            if (isset($options['repository'])) {
                $this->setRepository($options['repository']);
            }

            if (isset($options['key'])) {
                $this->setKey($options['key']);
            }

            if (isset($options['exclude'])) {
                $this->setExclude($options['exclude']);
            }
        }

        parent::__construct($options);
    }


    abstract public function setRepository($repository);

    abstract public function setKey($key);

    abstract public function setExclude($exclude);

}
