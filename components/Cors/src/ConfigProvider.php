<?php
namespace Cors;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencyConfig(),
        ];
    }


    public function getDependencyConfig()
    {
        return [
            'factories' => [
                Service\Cors::class => Service\Factory::class,
                Options\OptionsInterface::class => Options\Factory::class,
                Middleware\Cors::class => Middleware\Factory::class,
            ]
        ];
    }
}