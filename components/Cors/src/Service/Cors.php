<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the MIT license.
 */

namespace Cors\Service;

use Zend\Diactoros\Uri;
use Cors\Exception\DisallowedOriginException;
use Cors\Options\OptionsInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Service that offers a simple mechanism to handle CORS requests
 *
 * This service closely follow the specification here: https://developer.mozilla.org/en-US/docs/HTTP/Access_control_CORS
 *
 * @license MIT
 * @author  Florent Blaison <florent.blaison@gmail.com>
 */
class Cors
{
    /**
     * @var OptionsInterface
     */
    protected $options;

    /**
     * @param OptionsInterface $options
     */
    public function __construct(OptionsInterface $options)
    {
        $this->options = $options;
    }


    /**
     * Check if the HTTP request is a CORS request by checking if the Origin header is present and that the
     * request URI is not the same as the one in the Origin
     *
     * @param  ServerRequestInterface $request
     * @return bool
     */
    public function isCorsRequest(ServerRequestInterface $request)
    {
        if (!$request->hasHeader('Origin')) {
            return false;
        }

        $originHeader = $request->getHeaderLine('Origin');

        $originUri  = new Uri($originHeader);
        $requestUri = $request->getUri();

        // According to the spec (http://tools.ietf.org/html/rfc6454#section-4), we should check host, port and scheme

        return (!($originUri->getHost() === $requestUri->getHost())
            || !($originUri->getPort() === $requestUri->getPort())
            || !($originUri->getScheme() === $requestUri->getScheme())
        );
    }


    /**
     * Check if the CORS request is a preflight request
     *
     * @param  ServerRequestInterface $request
     * @return bool
     */
    public function isPreflightRequest(ServerRequestInterface $request)
    {
        return $this->isCorsRequest($request)
            && strtoupper($request->getMethod()) === 'OPTIONS'
            && $request->hasHeader('Access-Control-Request-Method');
    }

    /**
     * Create a preflight response by adding the corresponding headers
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface|static
     */
    public function createPreflightCorsResponse(ServerRequestInterface $request, ResponseInterface $response)
    {
        $response = $response
            ->withStatus(200)
            ->withHeader('Access-Control-Allow-Origin', $this->getAllowedOriginValue($request))
            ->withHeader('Access-Control-Allow-Methods', implode(', ', $this->options->getAllowedMethods()))
            ->withHeader('Access-Control-Allow-Headers', implode(', ', $this->options->getAllowedHeaders()))
            ->withHeader('Access-Control-Max-Age', $this->options->getMaxAge())
            ->withHeader('Content-Length', 0);

        if ($this->options->getAllowedCredentials()) {
            $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
        }

        return $response;
    }

    /**
     * Populate a simple CORS response
     *
     * @param  ServerRequestInterface   $request
     * @param  ResponseInterface        $response
     * @return ResponseInterface
     * @throws DisallowedOriginException If the origin is not allowed
     */
    public function populateCorsResponse(ServerRequestInterface $request, ResponseInterface $response)
    {
        $originHeader = $this->getAllowedOriginValue($request);

        // If $origin is "null", then it means than the origin is not allowed. As this is
        // a simple request, it is useless to continue the processing as it will be refused
        // by the browser anyway, so we throw an exception
        if ($originHeader === 'null') {
            throw new DisallowedOriginException(
                sprintf('The origin "%s" is not authorized', $request->getHeaderLine('Origin'))
            );
        }

        $response = $response
            ->withHeader('Access-Control-Allow-Origin', $originHeader)
            ->withHeader('Access-Control-Expose-Headers', implode(', ', $this->options->getExposedHeaders()));

        $response = $this->ensureVaryHeader($response);

        if ($this->options->getAllowedCredentials()) {
            $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
        }

        return $response;
    }

    /**
     * Get a single value for the "Access-Control-Allow-Origin" header
     *
     * According to the spec, it is not valid to set multiple origins separated by commas. Only accepted
     * value are wildcard ("*"), an exact domain or a null string.
     *
     * @link http://www.w3.org/TR/cors/#access-control-allow-origin-response-header
     * @param  ServerRequestInterface $request
     * @return string
     */
    protected function getAllowedOriginValue(ServerRequestInterface $request)
    {
        $allowedOrigins = $this->options->getAllowedOrigins();

        if (in_array('*', $allowedOrigins)) {
            return '*';
        }

        $originHeader = $request->getHeaderLine('Origin');

        if ($originHeader) {
            foreach ($allowedOrigins as $allowedOrigin) {
                if (fnmatch($allowedOrigin, $originHeader)) {
                    return $originHeader;
                }
            }
        }

        return 'null';
    }

    /**
     * Ensure that the Vary header is set.
     *
     *
     * @link http://www.w3.org/TR/cors/#resource-implementation
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function ensureVaryHeader(ResponseInterface $response)
    {
        //$headers = $response->getHeaders();
        // If the origin is not "*", we should add the "Origin" value to the "Vary" header
        // See more: http://www.w3.org/TR/cors/#resource-implementation
        $allowedOrigins = $this->options->getAllowedOrigins();

        if (in_array('*', $allowedOrigins)) {
            return $response;
        }

        $response = $response->withAddedHeader('Vary', 'Origin');

        return $response;
    }
}