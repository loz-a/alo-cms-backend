<?php
namespace Cors\Options;


interface OptionsInterface
{
    public function setAllowedOrigins(array $allowedOrigins);

    /**
     * @return array
     */
    public function getAllowedOrigins();


    /**
     * @param  array $allowedMethods
     * @return void
     */
    public function setAllowedMethods(array $allowedMethods);

    /**
     * @return array
     */
    public function getAllowedMethods();


    /**
     * @param  array $allowedHeaders
     * @return void
     */
    public function setAllowedHeaders(array $allowedHeaders);

    /**
     * @return array
     */
    public function getAllowedHeaders();


    /**
     * @param  int  $maxAge
     * @return void
     */
    public function setMaxAge($maxAge);

    /**
     * @return int
     */
    public function getMaxAge();


    /**
     * @param  array $exposedHeaders
     * @return void
     */
    public function setExposedHeaders(array $exposedHeaders);

    /**
     * @return array
     */
    public function getExposedHeaders();


    /**
     * @param  bool $allowedCredentials
     * @return void
     */
    public function setAllowedCredentials($allowedCredentials);

    /**
     * @return boolean
     */
    public function getAllowedCredentials();
}