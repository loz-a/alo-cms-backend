<?php
namespace Cors\Middleware;

use Cors\Service\Cors as CorsService;
use Interop\Container\ContainerInterface;

class Factory
{
   public function __invoke(ContainerInterface $container)
   {
       $corsService = $container->get(CorsService::class);

       return new Cors($corsService);
   }
}