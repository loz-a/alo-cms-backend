<?php
namespace Cors\Middleware;

use Cors\Exception\DisallowedOriginException;
use Cors\Service\Cors as CorsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmptyResponse;

class Cors
{
    /**
     * @var CorsService
     */
    protected $corsService;

    /**
     * Whether or not a preflight request was detected
     *
     * @var bool
     */
    protected $isPreflight = false;

    /**
     * @param CorsService $corsService
     */
    public function __construct(CorsService $corsService)
    {
        $this->corsService = $corsService;
    }


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ){
        $response = $this->onCorsPreflight($request, $response);

        $result = $next($request, $response);

        if ($result instanceof ResponseInterface) {
            $result = $this->onCorsRequest($request, $result);
        }

        return $result;
    }


    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function onCorsPreflight(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Reset state flag
        $this->isPreflight = false;

        if (!$this->corsService->isCorsRequest($request)) {
            return $response;
        }

        // If this isn't a preflight, done
        if (!$this->corsService->isPreflightRequest($request)) {
            return $response;
        }

        // Preflight -- return a response now!
        $this->isPreflight = true;

        return $this->corsService->createPreflightCorsResponse($request, $response);
    }


    public function onCorsRequest(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Do nothing if we previously created a preflight response
        if ($this->isPreflight) {
            return $response;
        }

        // Also ensure that the vary header is set when no origin is set
        // to prevent reverse proxy caching a wrong request; causing all of the following
        // requests to fail due to missing CORS headers.
        if (!$this->corsService->isCorsRequest($request)) {
            if (!$request->hasHeader('Origin')) {
                $response = $this->corsService->ensureVaryHeader($response);
            }
            return $response;
        }

        // This is the second step of the CORS request, and we let ZF continue
        // processing the response
        try {
            $response = $this->corsService->populateCorsResponse($request, $response);
        } catch (DisallowedOriginException $exception) {
            // Clear response for security
            $response = (new EmptyResponse())->withStatus(403, $exception->getMessage());
        }
        return $response;
    }

}