<?php

if (!function_exists('translate')) {

    function translate($message) { // marker for poedit
        return $message;
    }
}

if (!function_exists('__')) {

    function __($message) { // marker for poedit
        return $message;
    }
}