<?php

// Delegate static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server'
    && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))
) {
    return false;
}

chdir(dirname(__DIR__));
require 'vendor/autoload.php';
require __DIR__ . '/functions.php';

$container = require 'config/container.php';

$app = $container->get(\Zend\Expressive\Application::class);
$app->run();
