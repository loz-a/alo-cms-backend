<?php
return [
    'dependencies' => [
        'factories' => [
            Zend\Db\Adapter\AdapterInterface::class => Zend\Db\Adapter\AdapterServiceFactory::class,
        ],
    ],

    'db' => [
        'driver'  => 'pdo',
        'dsn'     => 'mysql:dbname=e-shop;host=localhost;charset=utf8',
        'user'    => 'root',
        'pass'    => 'AM23naRmysql',
    ],
];