<?php
return [
    'api' => [
//        'cors_headers' => [
//            'Access-Control-Allow-Origin' => '*',
//            'Access-Control-Request-Headers' => '*',
//            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
//            'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
//            'Access-Control-Allow-Credentials' => 'true',
//            'Access-Control-Max-Age' => 86400 // 24 hours
//        ],
        'jwt_options' => [
            'notBefore' => (time()), // Configures the time that the token can be used (nbf claim)
            'expiration' => (time() + 3600), // Configures the expiration time of the token (nbf claim)
        ]
    ]
];