<?php

use Zend\Expressive\ConfigManager\ConfigManager;
use Zend\Expressive\ConfigManager\PhpFileProvider;
use Zend\I18n\ConfigProvider as I18nConfigProvider;
use Zend\Form\ConfigProvider as FormConfigProvider;
use Cors\ConfigProvider as CorsCofigProvider;

use Admin\ConfigProvider as AdminConfigProvider;
use App\ConfigProvider as AppConfigProvider;
use Auth\ConfigProvider as AuthConfigProvider;
use Index\ConfigProvider as IndexConfigProvider;
use Options\ConfigProvider as OptionsConfigProvider;
use Languages\ConfigProvider as LanguagesConfigProvider;
use Translations\ConfigProvider as TranslationsConfigProvider;

$configManager = new ConfigManager([
    new PhpFileProvider('config/autoload/{{,*.}global,{,*.}local}.php'),
    I18nConfigProvider::class,
    FormConfigProvider::class,
    CorsCofigProvider::class,

    AppConfigProvider::class,
    IndexConfigProvider::class,
    AdminConfigProvider::class,
    AuthConfigProvider::class,
    OptionsConfigProvider::class,
    LanguagesConfigProvider::class,
    TranslationsConfigProvider::class,
]);

return new ArrayObject($configManager->getMergedConfig());
